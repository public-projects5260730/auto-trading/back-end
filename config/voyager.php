<?php

return [
    /*
    |--------------------------------------------------------------------------
    | User config
    |--------------------------------------------------------------------------
    |
    | Here you can specify voyager user configs
    |
    */

    'user' => [
        'add_default_role_on_register' => true,
        'default_role'                 => 'user',
        'default_avatar'               => 'users/default.png',
        'redirect'                     => '/admin',
    ],

    /*
    |--------------------------------------------------------------------------
    | Controllers config
    |--------------------------------------------------------------------------
    |
    | Here you can specify voyager controller settings
    |
    */

    'controllers' => [
        'namespace' => 'TCG\\Voyager\\Http\\Controllers',
    ],

    /*
    |--------------------------------------------------------------------------
    | Models config
    |--------------------------------------------------------------------------
    |
    | Here you can specify default model namespace when creating BREAD.
    | Must include trailing backslashes. If not defined the default application
    | namespace will be used.
    |
    */

    'models' => [
        //'namespace' => 'App\\Models\\',
    ],

    /*
    |--------------------------------------------------------------------------
    | Storage Config
    |--------------------------------------------------------------------------
    |
    | Here you can specify attributes related to your application file system
    |
    */

    'storage' => [
        'disk' => 'public',
    ],

    /*
    |--------------------------------------------------------------------------
    | Media Manager
    |--------------------------------------------------------------------------
    |
    | Here you can specify if media manager can show hidden files like(.gitignore)
    |
    */

    'hidden_files' => false,

    /*
    |--------------------------------------------------------------------------
    | Database Config
    |--------------------------------------------------------------------------
    |
    | Here you can specify voyager database settings
    |
    */

    'database' => [
        'tables' => [
            'hidden' => ['migrations', 'data_rows', 'data_types', 'menu_items', 'password_resets', 'permission_role', 'personal_access_tokens', 'settings'],
        ],
        'autoload_migrations' => true,
    ],

    /*
    |--------------------------------------------------------------------------
    | Multilingual configuration
    |--------------------------------------------------------------------------
    |
    | Here you can specify if you want Voyager to ship with support for
    | multilingual and what locales are enabled.
    |
    */

    'multilingual' => [
        /*
         * Set whether or not the multilingual is supported by the BREAD input.
         */
        'enabled' => true,

        /*
         * Select default language
         */
        'default' => 'en',

        /*
         * Select languages that are supported.
         */
        'locales' => [
            'en',
            'vi',
            //'pt',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Dashboard config
    |--------------------------------------------------------------------------
    |
    | Here you can modify some aspects of your dashboard
    |
    */

    'dashboard' => [
        // Add custom list items to navbar's dropdown
        'navbar_items' => [
            'voyager::generic.profile' => [
                'route'      => 'voyager.profile',
                'classes'    => 'class-full-of-rum',
                'icon_class' => 'voyager-person',
            ],
            'voyager::generic.home' => [
                'route'        => '/',
                'icon_class'   => 'voyager-home',
                'target_blank' => true,
            ],
            'voyager::generic.logout' => [
                'route'      => 'voyager.logout',
                'icon_class' => 'voyager-power',
            ],
        ],

        'widgets' => [
            // 'TCG\\Voyager\\Widgets\\UserDimmer',
            // 'TCG\\Voyager\\Widgets\\PostDimmer',
            // 'TCG\\Voyager\\Widgets\\PageDimmer',
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Automatic Procedures
    |--------------------------------------------------------------------------
    |
    | When a change happens on Voyager, we can automate some routines.
    |
    */

    'bread' => [
        // When a BREAD is added, create the Menu item using the BREAD properties.
        'add_menu_item' => true,

        // which menu add item to
        'default_menu' => 'admin',

        // When a BREAD is added, create the related Permission.
        'add_permission' => true,

        // which role add premissions to
        'default_role' => 'admin',
    ],

    /*
    |--------------------------------------------------------------------------
    | UI Generic Config
    |--------------------------------------------------------------------------
    |
    | Here you change some of the Voyager UI settings.
    |
    */

    'primary_color' => '#22A7F0',

    'show_dev_tips' => true, // Show development tip "How To Use:" in Menu and Settings

    // Here you can specify additional assets you would like to be included in the master.blade
    'additional_css' => [
        //'css/custom.css',
    ],

    'additional_js' => [


        // ============= tinymce plugins ============= 
        '/assets/libs/tinymce/plugins/advlist/plugin.min.js',
        '/assets/libs/tinymce/plugins/anchor/plugin.min.js',
        '/assets/libs/tinymce/plugins/autolink/plugin.min.js',
        '/assets/libs/tinymce/plugins/autoresize/plugin.min.js',
        '/assets/libs/tinymce/plugins/autosave/plugin.min.js',
        '/assets/libs/tinymce/plugins/charmap/plugin.min.js',
        // '/assets/libs/tinymce/plugins/code/plugin.min.js',
        '/assets/libs/tinymce/plugins/codesample/plugin.min.js',
        '/assets/libs/tinymce/plugins/directionality/plugin.min.js',
        // '/assets/libs/tinymce/plugins/emoticons/plugin.min.js',
        '/assets/libs/tinymce/plugins/fullscreen/plugin.min.js',
        '/assets/libs/tinymce/plugins/help/plugin.min.js',
        // '/assets/libs/tinymce/plugins/image/plugin.min.js',
        '/assets/libs/tinymce/plugins/importcss/plugin.min.js',
        '/assets/libs/tinymce/plugins/insertdatetime/plugin.min.js',
        // '/assets/libs/tinymce/plugins/link/plugin.min.js',
        // '/assets/libs/tinymce/plugins/lists/plugin.min.js',
        '/assets/libs/tinymce/plugins/media/plugin.min.js',
        '/assets/libs/tinymce/plugins/nonbreaking/plugin.min.js',
        '/assets/libs/tinymce/plugins/pagebreak/plugin.min.js',
        '/assets/libs/tinymce/plugins/preview/plugin.min.js',
        '/assets/libs/tinymce/plugins/quickbars/plugin.min.js',
        '/assets/libs/tinymce/plugins/save/plugin.min.js',
        '/assets/libs/tinymce/plugins/searchreplace/plugin.min.js',
        // '/assets/libs/tinymce/plugins/table/plugin.min.js',
        '/assets/libs/tinymce/plugins/template/plugin.min.js',
        '/assets/libs/tinymce/plugins/visualblocks/plugin.min.js',
        '/assets/libs/tinymce/plugins/visualchars/plugin.min.js',
        '/assets/libs/tinymce/plugins/wordcount/plugin.min.js',
    ],

    'googlemaps' => [
        'key'    => env('GOOGLE_MAPS_KEY', ''),
        'center' => [
            'lat' => env('GOOGLE_MAPS_DEFAULT_CENTER_LAT', '32.715738'),
            'lng' => env('GOOGLE_MAPS_DEFAULT_CENTER_LNG', '-117.161084'),
        ],
        'zoom' => env('GOOGLE_MAPS_DEFAULT_ZOOM', 11),
    ],

    // Activate compass when environment is NOT local
    'compass_in_production' => false,

    'media' => [
        // The allowed mimetypes to be uploaded through the media-manager.
        // 'allowed_mimetypes' => '*', //All types can be uploaded
        'allowed_mimetypes' => [
            'image/jpeg',
            'image/png',
            'image/gif',
            'image/bmp',
            'video/mp4',
        ],
        //Path for media-manager. Relative to the filesystem.
        'path'                => '/',
        'show_folders'        => true,
        'allow_upload'        => true,
        'allow_move'          => true,
        'allow_delete'        => true,
        'allow_create_folder' => true,
        'allow_rename'        => true,
        /*'watermark'           => [
            'source'         => 'watermark.png',
            'position'       => 'bottom-left',
            'x'              => 0,
            'y'              => 0,
            'size'           => 15,
       ],
       'thumbnails'          => [
           [
                'type'  => 'fit',
                'name'  => 'fit-500',
                'width' => 500,
                'height'=> 500
           ],
       ]*/
    ],
];
