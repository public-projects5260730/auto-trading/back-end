<?php

return [
    'trial' => [
        'enabled' => env('LICENSE_TRIAL_ENABLED', false),
        'account_number' => env('LICENSE_TRIAL_ACCOUNT_NUMBER', 1),
        'license_days' => env('LICENSE_TRIAL_LICENSE_DAYS', 3),
    ],
];
