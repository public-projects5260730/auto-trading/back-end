<?php

return [
    'providers' => [
        'paypal' => App\Payments\PayPal::class,
        'paypal-credit' => App\Payments\PayPalCredit::class,
        'stripe' => App\Payments\Stripe::class,
        'timebit' => App\Payments\Timebit::class,
    ],

    'purchasable' => [
        'license' => App\Purchasables\License::class
    ],

    'options' => [
        'paypal' => [
            'testMode' => env('PAYPAL_TEST_MODE', true),
            'primary_account' => env('PAYPAL_PRIMARY_ACCOUNT', ''),
            'alternate_accounts' => split(env('PAYPAL_ALTERNATE_ACCOUNTS', '')),
        ],
        'paypal-credit' => [
            'testMode' => env('PAYPAL_TEST_MODE', true),
            'primary_account' => env('PAYPAL_PRIMARY_ACCOUNT', ''),
            'alternate_accounts' => split(env('PAYPAL_ALTERNATE_ACCOUNTS', '')),
        ],
        'stripe' => [
            'publishable_key' => env('STRIPE_PUBLISHABLE_KEY', ''),
            'secret_key' => env('STRIPE_SECRET_KEY', ''),
            'signing_secret' => env('STRIPE_SIGNING_SECRET', ''),
        ],
        'timebit' => [
            'agent_code' => env('TIMEBIT_AGENT_CODE', ''),
            'agent_private_key' => env('TIMEBIT_AGENT_PRIVATE_KEY', ''),
            'timebit_public_key' => env('TIMEBIT_PUBLIC_KEY', '')
        ]
    ]
];
