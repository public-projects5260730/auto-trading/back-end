<?php

namespace App\Notifications\UserLicense\Emails;

use App\Models\UserLicense;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Storage;

class Purchased extends Notification
{
    use Queueable;

    /**
     * @var UserLicense
     */
    protected $userLicense;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(UserLicense $userLicense)
    {
        $this->userLicense = $userLicense;
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $userLicense = $this->userLicense;

        $mail = (new MailMessage);
        $mail->subject(Lang::get('emails.user_licenses.purchased.subject'));
        $mail->view('emails.user-licenses.purchased', [
            'userLicense' => $userLicense,
            'product' => $userLicense->product,
            'user' =>  $userLicense->user,
        ]);

        $botFiles = $userLicense->product->botFiles;
        if (!empty($botFiles) && is_array($botFiles)) {
            foreach ($botFiles as $file) {
                $filePath = Storage::disk(config('voyager.storage.disk'))->path($file['download_link']);
                $mail->attach($filePath, ['as' => $file['original_name']]);
            }
        }

        return $mail;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
