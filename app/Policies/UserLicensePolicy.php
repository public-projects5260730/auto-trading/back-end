<?php

namespace App\Policies;

use App\Models\UserLicense;
use App\Utils\Constants;
use Illuminate\Support\Arr;
use TCG\Voyager\Contracts\User;
use TCG\Voyager\Policies\BasePolicy;

class UserLicensePolicy extends BasePolicy
{
    protected function checkPermission(User $user, $model, $action)
    {
        if (in_array($action, ['delete'])) {
            return false;
        }
        return parent::checkPermission($user, $model, $action);
    }

    public function download(User $user, UserLicense $userLicense)
    {
        if (!$userLicense->license_key) {
            return false;
        }

        if (!in_array($userLicense->state, Arr::only(Constants::USER_LICENSE_STATES, ['NEW', 'ACTIVATED']))) {
            return false;
        }

        return true;
    }

    public function extend(User $user, UserLicense $userLicense)
    {
        return false;
    }

    public function upgrade(User $user, UserLicense $userLicense)
    {
        return false;
    }
}
