<?php

namespace App\Policies;

use TCG\Voyager\Contracts\User;
use TCG\Voyager\Policies\BasePolicy;

class LogPolicy extends BasePolicy
{
    protected function checkPermission(User $user, $model, $action)
    {
        if (!in_array($action, ['browse', 'read'])) {
            return false;
        }
        return parent::checkPermission($user, $model, $action);
    }
}
