<?php

namespace App\Policies;

use TCG\Voyager\Contracts\User;
use TCG\Voyager\Policies\BasePolicy;

class RolePolicy extends BasePolicy
{
    protected function checkPermission(User $user, $model, $action)
    {
        $roleIds = split(env('PRIVATE_ROLE_IDS', ''));
        if (in_array($action, ['delete']) && (!$model->exists || in_array($model->getKey(), $roleIds))) {
            return false;
        }
        return parent::checkPermission($user, $model, $action);
    }
}
