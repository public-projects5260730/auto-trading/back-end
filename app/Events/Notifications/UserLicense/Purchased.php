<?php

namespace App\Events\Notifications\UserLicense;

use App\Models\User;
use App\Models\UserLicense;
use Illuminate\Queue\SerializesModels;

class Purchased
{
    use SerializesModels;

    /**
     * @var UserLicense
     */
    public $userLicense;

    /**
     * @var User
     */
    public $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(UserLicense $userLicense)
    {
        $this->userLicense = $userLicense;
        $this->user = $userLicense->user;
    }
}
