<?php

namespace App\Purchasables;

use App\Models\Product;
use App\Payments\CallbackState;
use App\Services\UserLicense;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;

class License extends AbstractPurchasable
{
	public function getTitle()
	{
		return __('licenses.licenses');
	}

	/**
	 * Prepares all the data required to create the Purchase object.
	 *
	 * @param \App\Models\User $purchaser
	 * @param null $error
	 *
	 * @return Purchase
	 */

	public function getPurchaseFromRequest($purchaser, &$error = null)
	{
		$paymentData = $this->getPaymentData(request()->all(), $error);
		if (empty($paymentData)) {
			return false;
		}

		$paymentProfile = Arr::get($paymentData, 'paymentProfile');
		$purchasable = Arr::except($paymentData, ['paymentProfile']);

		return $this->getPurchaseObject($paymentProfile, $purchasable, $purchaser);
	}

	public function getPurchasableFromExtraData(array $extraData)
	{
		$output = [
			'title' => '',
			'purchasable' => null
		];

		$rules = [
			'license_id' => ['required', 'numeric'],
			'product_id' => ['required', 'numeric'],
			'email' => ['required', 'email', 'max:250'],
		];

		$validator = Validator::make($extraData, $rules);
		if ($validator->fails()) {
			return $output;
		}

		$validated = $validator->validated();

		$productId = Arr::get($validated, 'product_id', 0);
		$licenseId = Arr::get($validated, 'license_id', 0);
		$email = Arr::get($validated, 'email', '');

		$product = Product::query()->where('id', $productId)->published()->first();
		if (empty($product)) {
			return $output;
		}

		/** @var Collection $licenses */
		$licenses = $product->licenses;
		$license = $licenses->find($licenseId);
		if (empty($license)) {
			return $output;
		}

		$output['title'] = $product->title;
		$output['purchasable'] = [
			'product' => $product,
			'license' => $license,
			'email' => $email,
		];

		return $output;
	}

	/**
	 * @param \App\Models\User $purchaser
	 *
	 * @return Purchase
	 */
	public function getPurchaseFromExtraData(array $extraData, \App\Models\PaymentProfile $paymentProfile, $purchaser, &$error = null)
	{
		$paymentData = $this->getPaymentData($extraData, $error);
		if (empty($paymentData)) {
			return false;
		}

		$paymentProfile = Arr::get($paymentData, 'paymentProfile');
		$purchasable = Arr::except($paymentData, ['paymentProfile']);

		return $this->getPurchaseObject($paymentProfile, $purchasable, $purchaser);
	}

	protected function getPaymentData(array $data, &$error = null)
	{

		$rules = [
			'license_id' => ['required', 'numeric'],
			'product_id' => ['required', 'numeric'],
			'payment_profile_id' => ['required', 'numeric'],
			'email' => ['required', 'email', 'max:250'],
		];

		$validator = Validator::make($data, $rules);
		if ($validator->fails()) {
			$error = $validator->errors()->messages();
			return;
		}

		$validated = $validator->validated();

		$filterData = filter($validated, [
			'license_id' => 'uint',
			'product_id' => 'uint',
			'payment_profile_id' => 'uint',
			'email' => 'str'
		]);

		$productId = Arr::get($filterData, 'product_id');
		$licenseId = Arr::get($filterData, 'license_id');
		$profileId = Arr::get($filterData, 'payment_profile_id');
		$email = Arr::get($filterData, 'email');

		$product = Product::query()->where('id', $productId)->published()->first();
		if (empty($product)) {
			$error = __('request_product_not_found');
			return;
		}

		/** @var Collection $licenses */
		$licenses = $product->licenses;
		$license = $licenses->find($licenseId);
		if (empty($license)) {
			$error = __('request_license_not_found');
			return;
		}

		/** @var Collection $paymentProfiles */
		$paymentProfiles = $product->paymentProfiles;
		$paymentProfile = $paymentProfiles->find($profileId);
		if (!$paymentProfile || !$paymentProfile->active) {
			$error = __('licenses.please_choose_valid_payment_profile_to_continue_with_your_purchase');
			return;
		}

		return [
			'product' => $product,
			'license' => $license,
			'paymentProfile' => $paymentProfile,
			'email' => $email,
		];
	}

	/**
	 * @param \App\Models\PaymentProfile $paymentProfile
	 * @param \App\Models\User $purchaser
	 *
	 * @return Purchase
	 */
	public function getPurchaseObject(\App\Models\PaymentProfile $paymentProfile, $purchasable, $purchaser)
	{
		$product = Arr::get($purchasable, 'product', null);
		$license = Arr::get($purchasable, 'license', null);
		$email =  Arr::get($purchasable, 'email', '');

		$purchase = new Purchase();

		$purchase->title = $product->title . ' (' . $license->title . ')';
		$purchase->cost = $license->cost_amount;
		$purchase->currency = $license->cost_currency;
		$purchase->accountNumber = $license->account_number;
		$purchase->licenseDays = $license->license_days;
		$purchase->purchaser = $purchaser;
		$purchase->email = $email;
		$purchase->paymentProfile = $paymentProfile;
		$purchase->purchasableTypeId = $this->purchasableTypeId;
		$purchase->purchasableId = $product->getKey();
		$purchase->purchasableTitle = $product->title;
		$purchase->extraData = [
			'product_id' => $product->getKey(),
			'license_id' => $license->getKey(),
			'payment_profile_id' => $paymentProfile->getKey(),
			'license' => $license->toArray(),
			'email' => $email,
		];

		$purchase->returnUrl = route('purchases.purchased', ['type' => $this->purchasableTypeId]);
		$purchase->cancelUrl = route('products.view', ['slug' => $product->slug]);

		return $purchase;
	}

	public function completePurchase(CallbackState $state)
	{
		$purchaseRequest = $state->getPurchaseRequest();
		$userLicenseId = Arr::get($purchaseRequest->extra_data, 'user_license_id');

		$paymentResult = $state->paymentResult;

		/** @var UserLicense $userLicenseService */
		$userLicenseService = resolve(UserLicense::class);

		$userLicense = null;

		switch ($paymentResult) {
			case CallbackState::PAYMENT_RECEIVED:
				$user = $userLicenseService->createUserIfNeed($state->getPurchaser(), $purchaseRequest->extra_data);
				$userLicense = $userLicenseService->createOrUpgrade($user, $purchaseRequest->extra_data, $state->requestKey);

				$state->logType = 'payment';
				$state->logMessage = 'Payment received, create/extended.';
				break;

			case CallbackState::PAYMENT_REINSTATED:
				if ($userLicenseId) {
					$user = $userLicenseService->createUserIfNeed($state->getPurchaser(), $purchaseRequest->extra_data);
					$userLicense = $userLicenseService->createOrUpgrade($user, $purchaseRequest->extra_data, $state->requestKey);
					$state->logType = 'payment';
					$state->logMessage = 'Reversal cancelled, create/extended reactivated.';
				} else {
					$state->logType = 'info';
					$state->logMessage = 'OK, no action.';
				}
				break;
		}

		if ($userLicense && $purchaseRequest) {
			$extraData = $purchaseRequest->extra_data;
			$extraData['user_license_id'] = $userLicense->getKey();
			$purchaseRequest->extra_data = $extraData;
			$purchaseRequest->save();
		}
	}

	public function reversePurchase(CallbackState $state)
	{
		$purchaseRequest = $state->getPurchaseRequest();

		/** @var UserLicense $userLicenseService */
		$userLicenseService = resolve(UserLicense::class);
		$userLicenseService->cancelOrDowngrade($state->getPurchaser(), $purchaseRequest->extra_data, $state->requestKey);

		$state->logType = 'cancel';
		$state->logMessage = 'Payment refunded/reversed.';
	}

	public function getPurchasablesByProfileId($profileId)
	{
		// @todo callback from admin call
	}
}
