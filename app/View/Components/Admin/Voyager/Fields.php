<?php

namespace App\View\Components\Admin\Voyager;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\View\Component;
use Illuminate\Support\Str;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\DataType;

class Fields extends Component
{
    public $dataType;
    public $dataTypeContent;
    public $hideLabel;
    public $rows;
    public $edit;
    public $add;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(DataType $dataType, Model $dataTypeContent, $fields = [], $excludeFields = [], $hideLabel = false)
    {
        $this->dataType = $dataType;
        $this->dataTypeContent = $dataTypeContent;
        $this->hideLabel = !!$hideLabel;

        $edit = !is_null($dataTypeContent->getKey());

        $this->edit = $edit;
        $this->add = !$edit;

        /** @var Collection $rows */
        $rows = $edit ? $dataType->editRows : $dataType->addRows;
        if (!$rows || !$rows->count()) {
            return;
        }

        $fields =  $this->getFields($fields);
        $excludeFields =  $this->getFields($excludeFields);

        $this->rows = $rows->filter(function (DataRow $row) use ($fields, $excludeFields) {
            if (!empty($fields) && !in_array($row->field, $fields)) {
                return false;
            }

            if (!empty($excludeFields) && in_array($row->field, $excludeFields)) {
                return false;
            }

            return true;
        });
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        if (!$this->rows || !$this->rows->count()) {
            return;
        }

        return view('components.admin.voyager.fields');
    }

    protected function getFields($fields)
    {
        if (is_string($fields)) {
            $fields = split($fields);
        }

        if (!is_array($fields)) {
            $fields = [$fields];
        }

        return $fields;
    }
}
