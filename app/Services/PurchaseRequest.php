<?php

namespace App\Services;

use App\Models\PaymentProfile;
use App\Models\PurchaseRequest as ModelsPurchaseRequest;
use App\Models\User;
use App\Purchasables\Purchase;
use Illuminate\Support\Str;

class PurchaseRequest
{
    /**
     * @return ModelsPurchaseRequest
     */
    public function insertPurchaseRequest(Purchase $purchase)
    {
        /** @var User $purchaser */
        $purchaser = $purchase->purchaser;

        /** @var PaymentProfile $paymentProfile */
        $paymentProfile = $purchase->paymentProfile;

        $data = [
            'request_key' => $this->generateRequestKey(),
            'user_id' => $purchaser ? $purchaser->getKey() : 0,
            'provider_id' =>  $paymentProfile->provider_id,
            'payment_profile_id' => $paymentProfile->getKey(),
            'purchasable_type_id' => $purchase->purchasableTypeId,
            'cost_amount' => $purchase->cost,
            'cost_currency' => $purchase->currency,
            'extra_data' => $purchase->extraData
        ];

        $purchaseRequest = new ModelsPurchaseRequest();

        $purchaseRequest->mergeFillable(array_keys($data))
            ->fill($data)
            ->save();

        return $purchaseRequest;
    }

    public function generateRequestKey()
    {
        do {
            $requestKey = Str::random(32);
            $existing = ModelsPurchaseRequest::query()->where('request_key', $requestKey)->first();
            if (!$existing) {
                break;
            }
        } while (true);

        return $requestKey;
    }
}
