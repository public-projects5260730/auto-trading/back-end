<?php

namespace App\Services;

use App\Models\User;
use App\Models\UserLicense as ModelsUserLicense;
use App\Utils\Constants;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Password;

class UserLicense
{
    /**
     * @var User
     */
    protected $user;

    protected $extraData = [];

    /**
     * @param User $user
     */
    public function createTrialLicense($user, $productId = 0)
    {
        $trialLicense = config('licenses.trial');
        if (empty($trialLicense['enabled'])) {
            return;
        }

        $userLicense = ModelsUserLicense::query()->newModelInstance();

        /** @var ModelsUserLicense $userLicense */
        $userLicense->user_id = $user ? $user->getKey() : 0;
        $userLicense->email = $user ? $user->email : request()->get('email', '');

        $userLicense->product_id = $productId ?? 0;
        $userLicense->license_id = 0;

        $userLicense->account_number = (int) Arr::get($trialLicense, 'account_number', 1);
        $userLicense->license_days = (int) Arr::get($trialLicense, 'account_number', 3);
        $userLicense->is_trial = true;

        $userLicense->save();
        return $userLicense;
    }

    /**
     * @param User $user
     * @return User
     */
    public function createUserIfNeed($user, array $extraData)
    {
        if ($user || empty($extraData['email'])) {
            return $user;
        }

        $email = Arr::get($extraData, 'email');
        $existingUser = User::query()->where('email', $email)->first();

        if (!empty($existingUser)) {
            return $user;
        }

        $password = Str::random();

        $user = (new User())->forceFill([
            'name' => $email,
            'email' => $email,
            'password' => bcrypt($password),
        ]);

        $user->save();

        // @todo send email purchase to create user

        // send register email and link reset password
        event(new Registered($user));
        Password::sendResetLink(['email' => $email]);

        return $user;
    }

    /**
     * @param User $user
     */
    public function createOrUpgrade($user, array $extraData, $purchaseRequestKey = null)
    {
        $licenseId = Arr::get($extraData, 'license_id');
        $productId = Arr::get($extraData, 'product_id');
        $email = Arr::get($extraData, 'email');

        $accountNumber = Arr::get($extraData, 'license.account_number');
        $licenseDays = Arr::get($extraData, 'license.license_days');
        $isTrial = Arr::get($extraData, 'license.is_trial', false);

        if ($accountNumber === null || $licenseDays === null) {
            Log::info('Error UserLicense.createOrUpgrade', compact('user', 'extraData', 'purchaseRequestKey'));
            abort(403, 'Miss AccountNumber or LicenseDays in extra');
            return;
        }

        $upgradeUserLicenseId = Arr::get($extraData, 'upgrade_user_license_id');

        if (!empty($upgradeUserLicenseId)) {
            $states = Arr::only(Constants::USER_LICENSE_STATES, ['NEW', 'ACTIVATED']);

            $query = ModelsUserLicense::query();
            $query->where('id', $upgradeUserLicenseId);
            $query->whereIn('state', array_values($states));

            if ($user) {
                $query->where('user_id', $user->getKey());
            } else {
                $query->where('email', $email);
            }
            $userLicense = $query->firstOrNew();
        } else {
            $userLicense = ModelsUserLicense::query()->newModelInstance();
        }

        /** @var ModelsUserLicense $userLicense */

        if ($userLicense->exists) {
            if ($accountNumber < 0 || $userLicense->account_number < 0) {
                $userLicense->account_number = -1;
            } else {
                $userLicense->account_number = max(0, $userLicense->account_number, $accountNumber);
            }

            if ($licenseDays < 0 || $userLicense->license_days < 0) { // unlimit 
                $userLicense->license_days = -1;
            } elseif ($licenseId == $userLicense->license_id) { // extend
                $userLicense->license_days = max(0, $userLicense->license_days + $licenseDays);
            } else { // upgrade
                $userLicense->license_days = max(0, $userLicense->license_days, $licenseDays);
            }

            if ($userLicense->isDirty(['account_number', 'license_days'])) {
                $upgradeData = $userLicense->upgrade;
                if (empty($upgradeData) || !is_array($upgradeData)) {
                    $upgradeData = [];
                }

                $upgradeData + [
                    time() => $userLicense->getRawOriginal()
                ];

                $userLicense->upgrade = $upgradeData;
            }
        } else {
            if ($accountNumber < 0) {
                $userLicense->account_number = -1;
            } else {
                $userLicense->account_number = max(0, $accountNumber);
            }

            if ($licenseDays < 0) {
                $userLicense->license_days = -1;
            } else {
                $userLicense->license_days = max(0, $licenseDays);
            }
        }

        $userLicense->user_id = $user ? $user->getKey() : 0;
        $userLicense->email = $email;
        $userLicense->product_id = $productId ?? 0;
        $userLicense->license_id = $licenseId ?? 0;
        $userLicense->purchase_request_key = $purchaseRequestKey;

        $userLicense->is_trial = !empty($isTrial);

        $userLicense->save();
        return $userLicense;
    }

    /**
     * @param User $user
     */
    public function cancelOrDowngrade($user, array $extraData, $purchaseRequestKey = null)
    {
        $userLicenseId = Arr::get($extraData, 'user_license_id');
        $states = Arr::only(Constants::USER_LICENSE_STATES, ['NEW', 'ACTIVATED']);

        $query = ModelsUserLicense::query();
        $query->where('id', $userLicenseId)
            ->where('purchase_request_key', $purchaseRequestKey)
            ->whereIn('state', array_values($states));

        if ($user) {
            $query->where('user_id', $user->getKey());
        } else {
            $email = Arr::get($extraData, 'email', '');
            $query->where('email', $email);
        }

        /** @var ModelsUserLicense $userLicense */
        $userLicense = $query->first();

        if (empty($userLicense)) {
            Log::info('Error UserLicense.cancelOrDowngrade', compact('user', 'extraData', 'purchaseRequestKey'));
            return;
        }

        $upgradeData = $userLicense->upgrade;
        if (empty($upgradeData) || !is_array($upgradeData)) {
            $userLicense->state = Constants::USER_LICENSE_STATES['CANCELED'];
            $userLicense->reason = 'Refund';
            $userLicense->save();
            return;
        }

        ksort($upgradeData);
        $lastUpgrade = array_pop($upgradeData);

        $lastUpgradeData = Arr::only($lastUpgrade, [
            'license_id',
            'purchase_request_key',
            'account_number',
            'license_days',
            'is_trial'
        ]);

        $lastUpgradeData['upgrade'] = $upgradeData;

        $userLicense->mergeFillable(array_keys($lastUpgradeData))
            ->fill($lastUpgradeData)
            ->save();

        return $userLicense;
    }
}
