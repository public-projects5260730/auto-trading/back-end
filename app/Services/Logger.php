<?php

namespace App\Services;

use App\Models\Log;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use TCG\Voyager\Models\DataRow;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\MenuItem;
use TCG\Voyager\Models\Permission;
use TCG\Voyager\Models\Translation;

class Logger
{
    public function logger(Model $model, string $action, array $meta = null)
    {
        if ($model instanceof Log || $model instanceof DataRow || $model instanceof DataType) {
            return;
        }
        if ($model instanceof Translation || $model instanceof Permission || $model instanceof MenuItem) {
            return;
        }

        /** @var User $user */
        $user = auth()->user();

        if ($meta === null) {
            $meta = [
                'new'     => $action !== 'DELETED' ? $model->getAttributes() : null,
                'old'     => $action !== 'CREATED' ? $model->getOriginal()   : null,
                'changed' => $action === 'UPDATED' ? $model->getChanges()    : null,
            ];
        }

        $meta += [
            'user' => $user ? $user->only(['id', 'name', 'email']) : null
        ];

        foreach ($meta as &$data) {
            if (!empty($data) && is_array($data) && method_exists($model, 'getHidden')) {
                $data = Arr::except($data, $model->getHidden());
            }
        }
        unset($data);

        $logData = [
            'user_id' => $user ? $user->getKey() : 0,
            'table'   => $model->getTable(),
            'action'  => $action,
            'message' => $this->logSubject($model, $action),
            'meta'  => $meta
        ];

        $log = new Log();
        $log->mergeFillable(array_keys($logData))
            ->fill($logData)
            ->save();
    }

    public static function logSubject(Model $model, $action = null): string
    {
        return sprintf('%s:%s:%s', get_class($model), $action, $model->getKey());
    }
}
