<?php

namespace App\Services\Api;

use App\Models\UserLicense;
use App\Utils\Constants;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class BotLicense
{
    private $url;
    private $checkPass;

    public function __construct()
    {
        $this->url = config('services.botLicense.url');
        $this->checkPass = config('services.botLicense.checkPass');

        if (!$this->url) {
            throw new \Exception(__('configs.you_must_configs_bot_license_before_do_this_action'), Response::HTTP_FORBIDDEN);
        }
    }

    public function create(UserLicense $userLicense)
    {
        $botCode = Arr::get($userLicense, 'product.accountLive.bot_code', 'NULL');
        $sendData = [
            'ProductCode' => $botCode,
            'RequestCode' => $userLicense->purchase_request_key ?: Str::random(24),
            'IsTrial' => !empty($userLicense->is_trial),
            'AmountWorkingDay' => (int) $userLicense->license_days,
            'AmountAccount' => (int) $userLicense->account_number,
            'CheckPass' => $this->checkPass
        ];

        return $this->post('/license/create',  $sendData);
    }

    public function extend(UserLicense $userLicense)
    {
        $sendData = [
            'LicenseCode' => $userLicense->license_key,
            'IsTrial' => !empty($userLicense->is_trial),
            'AmountWorkingDay' => (int) $userLicense->license_days,
            'AmountAccount' => (int) $userLicense->account_number,
            'CheckPass' => $this->checkPass
        ];

        return $this->post('/license/extend',  $sendData);
    }

    public function cancel(UserLicense $userLicense)
    {
        $sendData = [
            'LicenseCode' => $userLicense->license_key,
            'CancelReasonContent' => $userLicense->reason ?: 'AutoTrading send canceled',
            'CheckPass' => $this->checkPass
        ];

        return $this->post('/license/cancel',  $sendData);
    }

    public function list(array $licenseKeys)
    {
        if (empty($licenseKeys)) {
            return;
        }

        $sendData = [
            'Licenses' => implode(',', $licenseKeys),
            'CheckPass' => $this->checkPass
        ];

        return $this->post('/license/checklicense',  $sendData);
    }

    private function post($action, $params)
    {
        $url = $this->url . $action;

        try {
            return Http::timeout(Constants::HTTP_TIMEOUT)->post($url, $params)->json();
        } catch (\Exception $error) {
            return [
                'error' => 'Call Bot License HTTP Error'
            ];
        }
    }
}
