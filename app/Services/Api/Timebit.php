<?php

namespace App\Services\Api;

use App\Utils\Constants;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Validation\Rule;

class Timebit
{
    private $url = 'https://payment.timebitotc.com';
    private $agentCode;
    private $agentPrivateKey;

    public function __construct(array $options)
    {
        if (empty($options['agent_code'])) {
            abort(Response::HTTP_FORBIDDEN, __('configs.payments.miss_timebit_agent_code'));
        }
        $this->agentCode = (int) Arr::get($options, 'agent_code');

        if (empty($options['agent_private_key'])) {
            abort(Response::HTTP_FORBIDDEN, __('configs.payments.miss_timebit_agent_private_key'));
        }
        $this->agentPrivateKey = Arr::get($options, 'agent_private_key');
    }

    public function createOrder(array $params, &$erors = null)
    {
        $validator = validator($params, [
            'order_id' => ['required', 'string', 'max:50'],
            'currency' => ['required', 'string', Rule::in(['TRX_USDT', 'VND'])],
            'amount' => ['required', 'numeric', 'min:1', 'max:1000000'],
            'redirect_url' => ['required', 'string'],
            'description' => ['required', 'string'],
            'expire_time' => ['required', 'numeric', Rule::in(range(5, 30, 5))],
            'order_link' => ['required', 'string'],
        ]);

        if ($validator->fails()) {
            $erors = $validator->errors()->messages();
            return;
        }

        $validated = $validator->validated();

        $params = filter($validated, [
            'order_id' => 'str',
            'currency' => 'str',
            'amount' => 'float',
            'redirect_url' => 'str',
            'description' => 'str',
            'expire_time' => 'uint',
            'order_link' => 'str',
        ]);

        $params += [
            'lang' => app()->getLocale(),
        ];

        return $this->post('/agent/create-order', $params);
    }

    private function post($action, $params)
    {
        $url = $this->url . $action;

        try {
            return Http::timeout(Constants::HTTP_TIMEOUT)
                ->accept('application/json')
                ->contentType('application/json')
                ->withHeaders($this->getHeaders($params))
                ->post($url, $params)->json();
        } catch (\Exception $error) {
            abort(403, $error->getMessage());
            return [
                'error' => 'Call Bot License HTTP Error'
            ];
        }
    }

    protected function getHeaders(array $params)
    {
        $headers = $params + [
            'agent_code' => $this->agentCode,
            'time' => Carbon::now()->getTimestampMs(),
        ];
        ksort($headers);

        $query = '';
        foreach ($headers as $key => $value) {
            $query .= "{$key}={$value}&";
        }

        openssl_sign($query, $signature, $this->agentPrivateKey, OPENSSL_ALGO_SHA256);
        $signature = base64_encode($signature);

        return [
            'agent-code' => $this->agentCode,
            'time' => Arr::get($headers, 'time'),
            'signature' => $signature,
        ];
    }
}
