<?php

namespace App\Helpers;

use Illuminate\Support\Arr;

class ArrayAccess
{
	private array $array;

	public function __construct(array $array)
	{
		$this->array = $array;
	}

	public function __get($key)
	{
		return Arr::get($this->array, $key);
	}
}
