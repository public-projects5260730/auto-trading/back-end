<?php

use App\Helpers\ArrayAccess;
use App\Helpers\DataFilterer;
use App\Payments\AbstractProvider;
use App\Purchasables\AbstractPurchasable;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Arr;
use Illuminate\Validation\Validator;

if (!function_exists('filter')) {
	/**
	 * Create a collection from the given value.
	 *
	 * @param  mixed  $value
	 * @return array|mixed
	 */
	function filter($data, $type, $nullable = false)
	{
		/** @var DataFilterer $dataFilter */
		$dataFilter = resolve('DataFilterer');
		if (is_array($type)) {
			/** @var Validator $validator */
			$validator = validator($data, $type);
			$rules = array_map(function ($_rules) {
				return implode('|', $_rules);
			}, $validator->getRules());

			$output = $dataFilter->filterArray($data, $rules, $nullable);
		} else {
			$output = $dataFilter->filter($data, $type);
		}

		if (!$nullable && is_array($output)) {
			return Arr::whereNotNull($output);
		}
		return $output;
	}
}

Arr::macro('getInArray', function ($value, array $values) {
	if (is_array($value)) {
		return array_intersect($value, $values);
	}
	if (in_array($value, $values)) {
		return $value;
	}
});

Arr::macro('errorJoin', function ($error, $glue = "\n") {
	if ($error === null || $error === '') {
		return;
	}
	if (is_scalar($error)) {
		return $error;
	}
	foreach ($error as &$val) {
		$val = Arr::errorJoin($val, $glue);
	}
	$error = Arr::whereNotNull($error);
	if (empty($error)) {
		return;
	}
	return implode($glue, array_unique($error));
});

Arr::macro('deepJoin', function ($array, $glue = "") {
	if (is_scalar($array) || $array === null) {
		return $array;
	}
	foreach ($array as &$val) {
		$val = Arr::deepJoin($val, $glue);
	}
	return implode($glue, $array);
});

Arr::macro('getAll', function ($array, $keys, $default = null) {
	if (is_array($keys)) {
		$values = [];
		foreach ($keys as $key) {
			$_default = $default;
			if (is_array($default)) {
				$_default = Arr::get($default, $key);
			}
			$values[$key] = Arr::get($array, $key, $_default);
		}
		return $values;
	}
	return Arr::get($array, $keys, $default);
});

Arr::macro('access', function (array $array) {
	return new ArrayAccess($array);
});

/**
 * ========= STRING ==========
 */
if (!function_exists('split')) {
	/**
	 * @param  mixed $subject
	 * @param  mixed $pattern
	 * @param  mixed $limit
	 * @param  mixed $flag
	 * @return array
	 */
	function split(string $subject, string $pattern = '/\s*[,;]+\s*/', int $limit = -1, int $flag = PREG_SPLIT_NO_EMPTY)
	{
		return preg_split($pattern, $subject, $limit, $flag);
	}
}

/**
 * ========= PAGINATOR ==========
 */
if (!function_exists('paginator')) {
	function paginator(\Illuminate\Pagination\LengthAwarePaginator $paginate)
	{
		return [
			'page' => $paginate->currentPage(),
			'perPage' => $paginate->perPage(),
			'total' => $paginate->total(),
			'resultCount' => $paginate->count(),
			'data' => $paginate->items(),
		];
	}
}

/**
 * ========= PAYMENTS ==========
 */

if (!function_exists('paymentProvider')) {
	/**
	 * @return AbstractProvider
	 */
	function paymentProvider(string $provider)
	{
		try {
			return resolve("payments.providers.{$provider}");
		} catch (\Throwable $th) {
			abort(403, __('requested.payment_provider_not_found'));
		}
	}
}

if (!function_exists('purchasable')) {
	/**
	 * @return AbstractPurchasable
	 */
	function purchasable(string $type)
	{
		try {
			return resolve("payments.purchasable.{$type}");
		} catch (\Throwable $th) {
			abort(403, __('requested.purchasable_not_found'));
		}
	}
}


/**
 * ========= QUERY DATABASE ==========
 */

if (!function_exists('query')) {

	/**
	 * @return Builder | EloquentBuilder
	 */
	function query($modelName)
	{
		if (method_exists($modelName, 'query')) {
			return $modelName::query();
		}
		return;
	}
}
