<?php

namespace App\Payments;

use App\Models\PaymentProfile;
use App\Models\PurchaseRequest;
use App\Models\User;

class CallbackState
{
	protected $purchaseRequest;
	protected $purchasableHandler;
	protected $paymentProfile;
	protected $purchaser;
	protected $paymentResult;

	const PAYMENT_RECEIVED = 1; // received payment
	const PAYMENT_REVERSED = 2; // refund/reversal
	const PAYMENT_REINSTATED = 3; // reversal cancelled

	/**
	 *
	 * @return PurchaseRequest | null
	 */
	public function getPurchaseRequest()
	{
		return $this->purchaseRequest;
	}

	/**
	 *
	 * @return \App\Purchasables\AbstractPurchasable
	 */
	public function getPurchasableHandler()
	{
		if ($this->purchasableHandler) {
			return $this->purchasableHandler;
		}

		$purchaseRequest = $this->getPurchaseRequest();
		if (!$purchaseRequest) {
			return false;
		}

		try {
			$purchasableHandler = resolve("payments.purchasable.{$purchaseRequest->purchasable_type_id}");
		} catch (\Exception $e) {
			return false;
		}

		$this->purchasableHandler = $purchasableHandler;
		return $this->purchasableHandler;
	}

	/**
	 *
	 * @return PaymentProfile | false
	 */
	public function getPaymentProfile()
	{
		if ($this->paymentProfile) {
			return $this->paymentProfile;
		}

		$purchaseRequest = $this->getPurchaseRequest();
		if (!$purchaseRequest) {
			return false;
		}
		$paymentProfile = PaymentProfile::query()->find($purchaseRequest->payment_profile_id);
		if (!$paymentProfile) {
			return false;
		}

		$this->paymentProfile = $paymentProfile;
		return $this->paymentProfile;
	}

	public function getPurchaser()
	{
		if ($this->purchaser) {
			return $this->purchaser;
		}

		$purchaseRequest = $this->purchaseRequest;
		if (!$purchaseRequest) {
			return false;
		}

		if (empty($purchaseRequest->user_id)) {
			return false;
		}

		$user = User::query()->find($purchaseRequest->user_id);
		if (!$user) {
			return false;
		}

		$this->purchaser = $user;
		return $this->purchaser;
	}

	function __get($name)
	{
		return isset($this->{$name}) ? $this->{$name} : null;
	}

	function __set($name, $value)
	{
		switch ($name) {
			case 'purchaseRequest':
				$this->purchaseRequest = $value;
				if ($value) {
					$this->requestKey = $value->request_key;
				}
				break;

			case 'requestKey':
				$this->purchaseRequest = PurchaseRequest::query()->where('request_key', $value)->first();
				$this->requestKey = $value;
				break;

			default:
				$this->{$name} = $value;
				break;
		}
	}
}
