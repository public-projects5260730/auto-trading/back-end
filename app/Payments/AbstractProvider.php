<?php

namespace App\Payments;

use App\Models\PaymentProfile;
use App\Models\PaymentProviderLog;
use App\Models\PurchaseRequest;
use App\Mvc\Controller;
use App\Purchasables\AbstractPurchasable;
use App\Purchasables\Purchase;

abstract class AbstractProvider
{
	protected $providerId;

	abstract public function getTitle();

	protected function getPaymentParams(PurchaseRequest $purchaseRequest, Purchase $purchase)
	{
		return [];
	}

	abstract public function initiatePayment(PurchaseRequest $purchaseRequest, Purchase $purchase);

	/**
	 * @return CallbackState
	 */
	abstract public function setupCallback();

	abstract public function getPaymentResult(CallbackState $state);

	abstract public function prepareLogData(CallbackState $state);

	public function __construct($providerId)
	{
		$this->providerId = $providerId;
	}

	public function renderConfig(PaymentProfile $profile)
	{
		$data = [
			'profile' => $profile
		];

		$viewName = "admin.payments.{$this->providerId}.config";
		if (view()->exists($viewName)) {
			return view($viewName, $data);
		}
	}

	public function verifyConfig(array &$options, &$errors = [])
	{
		return true;
	}

	public function processPayment(PurchaseRequest $purchaseRequest, PaymentProfile $paymentProfile, Purchase $purchase)
	{
		return null;
	}

	public function renderCancellationTemplate(PurchaseRequest $purchaseRequest)
	{
		return '';
	}

	protected function renderCancellationDefault(PurchaseRequest $purchaseRequest)
	{
		$data = [
			'purchaseRequest' => $purchaseRequest
		];

		$viewName = "payments.cancel_recurring";
		if (view()->exists($viewName)) {
			return view($viewName, $data);
		}
	}

	/**
	 * @param Controller $controller
	 * @param PurchaseRequest $purchaseRequest
	 * @param PaymentProfile $paymentProfile
	 *
	 * @return \App\Mvc\Reply\AbstractReply
	 */
	public function processCancellation(PurchaseRequest $purchaseRequest, PaymentProfile $paymentProfile)
	{
		throw new \LogicException("processCancellation must be overridden.");
	}

	public function validateCallback(CallbackState $state)
	{
		return true;
	}

	public function validateTransaction(CallbackState $state)
	{
		$total = PaymentProviderLog::query()
			->filters([
				'transaction_id' => $state->transactionId,
				'provider_id' => $this->providerId,
				'log_type' => ['payment', 'cancel'],
			])
			->orderBy('created_at', 'ASC')
			->count();


		if ($total) {
			$state->logType = 'info';
			$state->logMessage = 'Transaction already processed. Skipping.';
			return false;
		}
		return true;
	}

	public function validatePurchaseRequest(CallbackState $state)
	{
		if (!$state->getPurchaseRequest()) {
			$state->logType = 'error';
			$state->logMessage = 'Invalid purchase request.';
			return false;
		}
		return true;
	}

	/**
	 * @param  mixed $state
	 * @return AbstractPurchasable
	 */
	public function validatePurchasableHandler(CallbackState $state)
	{
		if (!$state->getPurchasableHandler()) {
			$state->logType = 'error';
			$state->logMessage = 'Could not find handler for purchasable type \'' . $state->getPurchaseRequest()->purchasable_type_id  . '\'.';
			return false;
		}
		return true;
	}

	public function validatePaymentProfile(CallbackState $state)
	{
		if (!$state->getPaymentProfile()) {
			$state->logType = 'error';
			$state->logMessage = 'Could not find a matching payment profile.';
			return false;
		}
		return true;
	}

	public function validatePurchaser(CallbackState $state)
	{
		$handler = $state->getPurchasableHandler();

		if (!$handler->validatePurchaser($state, $error)) {
			$state->logType = 'error';
			$state->logMessage = $error;
			return false;
		}
		return true;
	}

	public function validatePurchasableData(CallbackState $state)
	{
		return true;
	}

	public function validateCost(CallbackState $state)
	{
		return true;
	}

	public function setProviderMetadata(CallbackState $state)
	{
		return;
	}

	public function completeTransaction(CallbackState $state)
	{
		$purchasableHandler = $state->getPurchasableHandler();

		switch ($state->paymentResult) {
			case CallbackState::PAYMENT_RECEIVED:
				$purchasableHandler->completePurchase($state);
				break;

			case CallbackState::PAYMENT_REINSTATED:
				$purchasableHandler->completePurchase($state);
				break;

			case CallbackState::PAYMENT_REVERSED:
				$purchasableHandler->reversePurchase($state);
				break;

			default:
				$state->logType = 'info';
				$state->logMessage = 'OK, no action';
				break;
		}

		$purchasableHandler->postCompleteTransaction($state);
	}

	public function log(CallbackState $state)
	{
		$this->prepareLogData($state);

		$logData = [
			'purchase_request_key' => $state->requestKey,
			'provider_id' => $this->providerId,
			'transaction_id' => $state->transactionId,
			'log_type' => !empty($state->logType) ? $state->logType : 'error',
			'log_message' => $state->logMessage,
			'log_details' => $state->logDetails,
			'subscriber_id' => $state->subscriberId,
		];

		$log = new PaymentProviderLog();
		$log->mergeFillable(array_keys($logData))
			->fill($logData)
			->save();
	}

	public function verifyCurrency(PaymentProfile $paymentProfile, $currencyCode)
	{
		return true;
	}

	public function getCallbackUrl()
	{
		return route('payments.callback', ['providerId' => $this->providerId]);
	}

	public function getApiEndpoint()
	{
		return '';
	}

	public function getProviderId()
	{
		return $this->providerId;
	}

	protected function getProviderOptions()
	{
		$providerId = $this->providerId;
		$options = config("payments.options.{$providerId}", null);
		if ($options === null) {
			throw new \LogicException(__('provider_x_not_have_options', ['provider' => $providerId]));
		}
		return $options;
	}
}
