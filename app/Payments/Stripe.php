<?php

namespace App\Payments;

use App\Models\PurchaseRequest;
use App\Purchasables\Purchase;
use Illuminate\Support\Arr;
use Stripe\Checkout\Session;
use Stripe\Stripe as StripeStripe;
use Stripe\Webhook;

class Stripe extends AbstractProvider
{
	public function getTitle()
	{
		return 'Stripe';
	}

	public function initiatePayment(PurchaseRequest $purchaseRequest, Purchase $purchase)
	{
		$this->setupStripe();

		$session = Session::create([
			'line_items' => [[
				'price_data' => [
					'currency' => $purchase->currency,
					'product_data' => [
						'name' => $purchase->title,
					],
					'unit_amount' => $this->prepareCost($purchase->cost, $purchase->currency),
				],
				'quantity' => 1,
			]],
			'invoice_creation' => [
				'enabled' => true,
				'invoice_data' => [
					'metadata' => $this->getTransactionMetadata($purchaseRequest, $purchase)
				]
			],
			'mode' => 'payment',
			'success_url' => $purchase->returnUrl,
			'cancel_url' => $purchase->cancelUrl,
		], []);

		return response()->json([
			'redirect' => $session->url
		]);
	}

	protected function createPaymentIntent(PurchaseRequest $purchaseRequest, Purchase $purchase, &$error = null)
	{
		$paymentIntent = \Stripe\PaymentIntent::create([
			'amount' => $this->prepareCost($purchase->cost, $purchase->currency),
			'currency' => $purchase->currency,
			'description' => $purchase->title,
			'receipt_email' => $purchase->email,
			'metadata' => $this->getTransactionMetadata($purchaseRequest, $purchase) + [
				'type' => 'payment_intent'
			],
		]);

		return $paymentIntent;
	}

	protected function getTransactionMetadata(PurchaseRequest $purchaseRequest, Purchase $purchase)
	{
		return [
			'purchasable_type_id' => $purchase->purchasableTypeId,
			'purchasable_id' => $purchase->purchasableId,
			'currency' => $purchase->currency,
			'cost' => $purchase->cost,
			'account_number' => $purchase->accountNumber,
			'license_days' => $purchase->licenseDays,
			'request_key' => $purchaseRequest->request_key,
		];
	}

	/**
	 * 
	 * @return CallbackState
	 */
	public function setupCallback()
	{
		$state = new CallbackState();

		$state->signature = !empty($_SERVER['HTTP_STRIPE_SIGNATURE']) ? $_SERVER['HTTP_STRIPE_SIGNATURE'] : request()->header('HTTP_STRIPE_SIGNATURE');

		$filtered = filter(request()->all(), [
			'data' => 'array',
			'id' => 'str',
			'type' => 'str'
		]);

		$state->transactionId = $filtered['id'];
		$state->eventType = $filtered['type'];
		$state->event = Arr::get($filtered, 'data.object', []);

		$requestKey = Arr::get($filtered, 'data.object.metadata.request_key');
		if (!empty($requestKey)) {
			$state->requestKey = $requestKey;
		}
		return $state;
	}

	public function validateCallback(CallbackState $state)
	{
		if ($this->isEventSkippable($state)) {
			$state->httpCode = 200;
			return false;
		}

		if (!$this->validateExpectedValues($state)) {
			$state->logType = 'error';
			$state->logMessage = 'Event data received from Stripe does not contain the expected values.';
			if (!$state->requestKey) {
				$state->httpCode = 200; // Not likely to recover from this error so send a successful response.
			}
			return false;
		}

		if (!$this->verifyWebhookSignature($state)) {
			$state->logType = 'error';
			$state->logMessage = 'Webhook received from Stripe could not be verified as being valid.';
			$state->httpCode = 400;

			return false;
		}

		return true;
	}

	protected function getActionableEvents()
	{
		return [
			'charge.dispute.funds_withdrawn',
			'charge.dispute.funds_reinstated',
			'charge.refunded',
			'charge.succeeded',
			'invoice.payment_succeeded',
			'review.closed'
		];
	}

	protected function isEventSkippable(CallbackState $state)
	{
		$eventType = $state->eventType;

		if (!in_array($eventType, $this->getActionableEvents())) {
			return true;
		}

		if ($eventType === 'invoice.payment_succeeded' && (array_key_exists('charge', $state->event) && $state->event['charge'] === null)) {
			return true;
		}

		return false;
	}

	protected function validateExpectedValues(CallbackState $state)
	{
		return ($state->getPurchaseRequest() && $state->getPaymentProfile());
	}

	protected function verifyWebhookSignature(CallbackState $state)
	{
		$options = $this->getProviderOptions();
		$signingSecret = Arr::get($options, 'signing_secret');
		if (empty($signingSecret)) {
			return true;
		}

		if (empty($state->signature)) {
			return false;
		}

		$signature = $state->signature;

		try {
			$this->setupStripe();
			$payload = @file_get_contents('php://input');
			$verifiedEvent = Webhook::constructEvent($payload, $signature, $signingSecret);
		} catch (\Throwable $th) {
			return false;
		}

		return $verifiedEvent;
	}

	public function validateCost(CallbackState $state)
	{
		$purchaseRequest = $state->getPurchaseRequest();

		$currency = $purchaseRequest->cost_currency;
		$cost = $this->prepareCost($purchaseRequest->cost_amount, $currency);

		$amountPaid = null;

		switch ($state->eventType) {
			case 'charge.succeeded':
				$amountPaid = $state->event['amount'];
				break;
			case 'invoice.payment_succeeded':
				$amountPaid = $state->event['amount_paid'];
				break;
		}

		if ($amountPaid !== null) {
			$costValidated = ($amountPaid === $cost
				&& strtoupper($state->event['currency']) === $currency
			);

			if (!$costValidated) {
				$state->logType = 'error';
				$state->logMessage = 'Invalid cost amount';
				return false;
			}

			return true;
		}

		return true;
	}

	public function getPaymentResult(CallbackState $state)
	{
		switch ($state->eventType) {
			case 'charge.succeeded':
				if ($state->event['outcome']['type'] == 'authorized') {
					$state->paymentResult = CallbackState::PAYMENT_RECEIVED;
				} else {
					$state->logType = 'info';
					$state->logMessage = 'Charge succeeded but not authorized, it may require review in the Stripe Dashboard.';
				}

				// sleep for 5 seconds to offset an insanely fast webhook
				// being processed before the user is redirected
				usleep(5 * 1000000);

				$purchaseRequest = $state->purchaseRequest;

				if (
					$purchaseRequest
					&& $purchaseRequest->provider_metadata
					&& strpos($purchaseRequest->provider_metadata, 'sub_') === 0
					&& !empty($state->event['payment_method'])
				) {
					try {
						/** @var \Stripe\Subscription $subscription */
						$subscription = \Stripe\Subscription::retrieve(
							$purchaseRequest->provider_metadata
						);

						/** @var \Stripe\PaymentMethod $paymentMethod */
						$paymentMethod = \Stripe\PaymentMethod::retrieve(
							$state->event['payment_method']
						);

						\Stripe\Subscription::update($subscription->id, [
							'default_payment_method' => $paymentMethod->id
						]);
					} catch (\Throwable $e) {
					}
				}

				break;

			case 'invoice.payment_succeeded':
				$state->paymentResult = CallbackState::PAYMENT_RECEIVED;
				break;

			case 'review.closed':
				if ($state->event['reason'] == 'approved') {
					$state->paymentResult = CallbackState::PAYMENT_RECEIVED;
				} else {
					$state->logType = 'info';
					$state->logMessage = 'Previous payment review now closed, but not approved.';
				}
				break;

			case 'charge.refunded':
			case 'charge.dispute.funds_withdrawn':
				$state->paymentResult = CallbackState::PAYMENT_REVERSED;
				break;

			case 'charge.dispute.funds_reinstated':
				$state->paymentResult = CallbackState::PAYMENT_REINSTATED;
				break;
		}
	}

	public function prepareLogData(CallbackState $state)
	{
		$state->logDetails = request()->all();
	}

	protected $zeroDecimalCurrencies = [
		'BIF', 'CLP', 'DJF', 'GNF', 'JPY',
		'KMF', 'KRW', 'MGA', 'PYG', 'RWF',
		'VND', 'VUV', 'XAF', 'XOF', 'XPF'
	];

	/**
	 *
	 * @param $cost
	 * @param $currency
	 *
	 * @return int
	 */
	protected function prepareCost($cost, $currency)
	{
		if (!in_array(strtoupper($currency), $this->zeroDecimalCurrencies)) {
			$cost *= 100;
		}
		return intval($cost);
	}

	protected function setupStripe()
	{
		$options = $this->getProviderOptions();
		$secretKey = Arr::get($options, 'secret_key');
		StripeStripe::setApiKey($secretKey);
		StripeStripe::setApiVersion('2022-11-15');
	}
}
