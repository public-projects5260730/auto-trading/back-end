<?php

namespace App\Payments;

use App\Models\PurchaseRequest;
use App\Purchasables\Purchase;

class PayPalCredit extends PayPal
{
	public function getTitle()
	{
		return 'PayPal Credit';
	}

	protected function getPaymentParams(PurchaseRequest $purchaseRequest, Purchase $purchase)
	{
		$params = parent::getPaymentParams($purchaseRequest, $purchase);

		$params += [
			'landing_page' => 'billing'
		];

		return $params;
	}
}
