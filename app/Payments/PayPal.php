<?php

namespace App\Payments;

use App\Models\PaymentProviderLog;
use App\Models\PurchaseRequest;
use App\Purchasables\Purchase;
use App\Utils\Constants;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Http;

class PayPal extends AbstractProvider
{
	public function getTitle()
	{
		return 'PayPal';
	}

	public function getApiEndpoint()
	{
		$options = $this->getProviderOptions();
		$testMode = Arr::get($options, 'testMode', false);
		if (!$testMode) {
			return 'https://www.paypal.com/cgi-bin/webscr';
		} else {
			return 'https://www.sandbox.paypal.com/cgi-bin/webscr';
		}
	}

	protected function getPaymentParams(PurchaseRequest $purchaseRequest, Purchase $purchase)
	{
		$purchaser = $purchase->purchaser;

		$options = $this->getProviderOptions();

		$params = [
			'business' => Arr::get($options, 'primary_account', ''),
			'currency_code' => $purchase->currency,
			'item_name' => $purchase->title,
			'quantity' => 1,
			'no_note' => 1,

			'no_shipping' => 1,

			'custom' => $purchaseRequest->request_key,

			'charset' => 'utf8',
			'email' => $purchase->email,

			'return' => $purchase->returnUrl,
			'cancel_return' => $purchase->cancelUrl,
			'notify_url' => $this->getCallbackUrl(),
		];

		$params['cmd'] = '_xclick';
		$params['amount'] = $purchase->cost;

		return $params;
	}

	public function initiatePayment(PurchaseRequest $purchaseRequest, Purchase $purchase)
	{
		$params = $this->getPaymentParams($purchaseRequest, $purchase);

		$endpointUrl = $this->getApiEndpoint();
		$endpointUrl .= '?' . http_build_query($params);
		return response()->json([
			'redirect' => $endpointUrl
		]);
	}

	public function renderCancellationTemplate(PurchaseRequest $purchaseRequest)
	{
		$data = [
			'purchaseRequest' => $purchaseRequest,
			'endpoint' => $this->getApiEndpoint()
		];
		return view('payments.paypal.cancel_recurring', $data);
	}

	/**
	 * 
	 * @return CallbackState
	 */
	public function setupCallback()
	{
		$state = new CallbackState();

		$requestData = filter(request()->all(), [
			'business' => 'str',
			'receiver_email' => 'str',
			'txn_type' => 'str',
			'parent_txn_id' => 'str',
			'txn_id' => 'str',
			'subscr_id' => 'str',
			'residence_country' => 'str',
			'mc_gross' => 'unum',
			'tax' => 'unum',
			'mc_currency' => 'str',
			'payment_status' => 'str',
			'custom' => 'str',
			'test_ipn' => 'bool',
		]);

		$state->business = Arr::get($requestData, 'business');
		$state->receiverEmail = Arr::get($requestData, 'receiver_email');
		$state->transactionType = Arr::get($requestData, 'txn_type');
		$state->parentTransactionId = Arr::get($requestData, 'parent_txn_id');
		$state->transactionId = Arr::get($requestData, 'txn_id');
		$state->subscriberId = Arr::get($requestData, 'subscr_id');
		$state->paymentCountry = Arr::get($requestData, 'residence_country');
		$state->costAmount = Arr::get($requestData, 'mc_gross');
		$state->taxAmount = Arr::get($requestData, 'tax');
		$state->costCurrency = Arr::get($requestData, 'mc_currency');
		$state->paymentStatus = Arr::get($requestData, 'payment_status');

		$state->requestKey = Arr::get($requestData, 'custom');
		$state->testMode = Arr::get($requestData, 'test_ipn');

		$options = $this->getProviderOptions();
		$isTestMode = Arr::get($options, 'testMode', false);
		if (!$isTestMode) {
			$state->testMode = false;
		}

		$state->ip = request()->ip();
		$state->_POST = request()->all();

		return $state;
	}

	public function validateCallback(CallbackState $state)
	{
		try {
			$params = $state->_POST + ['cmd' => '_notify-validate'];
			if ($state->testMode) {
				$url = 'https://ipnpb.sandbox.paypal.com/cgi-bin/webscr';
			} else {
				$url = 'https://ipnpb.paypal.com/cgi-bin/webscr';
			}
			$response = Http::timeout(Constants::HTTP_TIMEOUT)->asForm()->post($url, $params);
			if (!$response || $response->body() != 'VERIFIED' || $response->status() != 200) {
				$state->logType = false;
				$state->logMessage = 'Request not validated (from unknown source)';
				return false;
			}
		} catch (\GuzzleHttp\Exception\RequestException $e) {
			$state->logType = 'error';
			$state->logMessage = 'Connection to PayPal failed: ' . $e->getMessage();
			return false;
		}

		return true;
	}

	public function validateTransaction(CallbackState $state)
	{
		if (!$state->requestKey) {
			$state->logType = 'info';
			$state->logMessage = 'No purchase request key. Unrelated payment, no action to take.';
			return false;
		}

		if (!$state->getPurchaseRequest()) {
			$state->logType = 'info';
			$state->logMessage = 'Invalid request key. Unrelated payment, no action to take.';
			return false;
		}

		if (!$state->transactionId && !$state->subscriberId) {
			$state->logType = 'info';
			$state->logMessage = 'No transaction or subscriber ID. No action to take.';
			return false;
		}

		$matchingLogs = PaymentProviderLog::query()
			->filters([
				'transaction_id' => $state->transactionId,
				'provider_id' => $state->providerId,
				'log_type' => ['payment', 'cancel'],
			])
			->orderBy('created_at', 'ASC')
			->get();

		if ($matchingLogs->count()) {
			foreach ($matchingLogs as $log) {
				if ($log->log_type == 'cancel' && $state->paymentStatus == 'Canceled_Reversal') {
					return true;
				}
			}

			$state->logType = 'info';
			$state->logMessage = 'Transaction already processed. Skipping.';
			return false;
		}

		return true;
	}

	public function validatePurchaseRequest(CallbackState $state)
	{
		return true;
	}

	public function validatePurchasableData(CallbackState $state)
	{
		$business = strtolower($state->business);
		$receiverEmail = strtolower($state->receiverEmail);

		$options = $this->getProviderOptions();
		$accounts = Arr::get($options, 'alternate_accounts', []);
		$accounts[] = Arr::get($options, 'primary_account', '');

		$matched = false;
		foreach ($accounts as $account) {
			$account = trim(strtolower($account));
			if ($account && ($business == $account || $receiverEmail == $account)) {
				$matched = true;
				break;
			}
		}
		if (!$matched) {
			$state->logType = 'error';
			$state->logMessage = 'Invalid business or receiver_email.';
			return false;
		}

		return true;
	}

	public function validateCost(CallbackState $state)
	{
		$purchaseRequest = $state->getPurchaseRequest();
		$cost = $purchaseRequest->cost_amount;
		$currency = $purchaseRequest->cost_currency;

		switch ($state->transactionType) {
			case 'web_accept':
			case 'subscr_payment':
				$costValidated = (round(($state->costAmount - $state->taxAmount), 2) == round($cost, 2)
					&& $state->costCurrency == $currency
				);

				if (!$costValidated) {
					$state->logType = 'error';
					$state->logMessage = 'Invalid cost amount';
					return false;
				}
		}
		return true;
	}

	public function getPaymentResult(CallbackState $state)
	{
		switch ($state->transactionType) {
			case 'web_accept':
			case 'subscr_payment':
				if ($state->paymentStatus == 'Completed') {
					$state->paymentResult = CallbackState::PAYMENT_RECEIVED;
				}
				break;
		}

		if ($state->paymentStatus == 'Refunded' || $state->paymentStatus == 'Reversed') {
			$state->paymentResult = CallbackState::PAYMENT_REVERSED;
		} else if ($state->paymentStatus == 'Canceled_Reversal') {
			$state->paymentResult = CallbackState::PAYMENT_REINSTATED;
		}
	}

	public function prepareLogData(CallbackState $state)
	{
		$state->logDetails = $state->_POST;
	}
}
