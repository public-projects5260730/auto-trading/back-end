<?php

namespace App\Payments;

use App\Models\PurchaseRequest;
use App\Purchasables\Purchase;
use App\Services\Api\Timebit as ApiTimebit;
use Illuminate\Support\Arr;

class Timebit extends AbstractProvider
{
	public function getTitle()
	{
		return 'Timebit OTC';
	}

	public function initiatePayment(PurchaseRequest $purchaseRequest, Purchase $purchase)
	{
		$timebitApi = $this->getTimebitApi();

		$params = [
			'order_id' => $purchaseRequest->request_key,
			'currency' => strtoupper($purchase->currency) == 'USD' ? 'TRX_USDT' : strtoupper($purchase->currency),
			'amount' => $purchase->cost,
			'redirect_url' => $purchase->returnUrl,
			'description' => $purchase->title,
			'expire_time' => 10,
			'order_link' => $purchase->cancelUrl
		];
		$response = $timebitApi->createOrder($params, $errors);
		if (empty($response) && !empty($errors)) {
			return response()->json(compact('errors'));
		}
		if (!empty($response['status'])) {
			$link = Arr::get($response, 'data.0.link');
			if (!empty($link)) {
				return response()->json([
					'redirect' => $link
				]);
			}
		}

		return response()->json([
			'error' => Arr::get($response, 'msg') ?: __('permissions.do_not_have_permission')
		]);
	}

	/**
	 * @return CallbackState
	 */
	public function setupCallback()
	{
		$state = new CallbackState();

		$filtered = filter(request()->all(), [
			'id' => 'str',
			'amount' => 'float',
			'currency' => 'str',
			'method' => 'str',
			'time' => 'unum',
			'description' => 'str',
			'from' => 'str',
			'to' => 'str',
			'type' => 'str',
			'status' => 'str',
			'hash' => 'str',
			'agentCode' => 'str',
			'fee' => 'float',
			'signature' => 'str',
		]);

		$state->transactionId = $filtered['id'];
		foreach ($filtered as $key => $value) {
			$state->{$key} = $value;
		}

		$requestKey = Arr::get($filtered, 'id');
		if (!empty($requestKey)) {
			$state->requestKey = $requestKey;
		}
		return $state;
	}

	public function validateCallback(CallbackState $state)
	{
		if (!$this->validateExpectedValues($state)) {
			$state->logType = 'error';
			$state->logMessage = 'Event data received from Timebit OTC does not contain the expected values.';
			if (!$state->requestKey) {
				$state->httpCode = 200;
			}
			return false;
		}

		if (!$this->verifyWebhookSignature($state)) {
			$state->logType = 'error';
			$state->logMessage = 'Webhook received from Timebit OTC could not be verified as being valid.';
			$state->httpCode = 400;

			return false;
		}

		return true;
	}

	protected function validateExpectedValues(CallbackState $state)
	{
		return ($state->getPurchaseRequest() && $state->getPaymentProfile());
	}

	protected function verifyWebhookSignature(CallbackState $state)
	{
		$options = $this->getProviderOptions();
		$timebitPublicKey = Arr::get($options, 'timebit_public_key');
		if (empty($timebitPublicKey)) {
			return false;
		}

		if (empty($state->signature)) {
			return false;
		}

		$signature = $state->signature;

		try {
			$verify = openssl_verify($this->getDataToVerifySignature(), base64_decode($signature), $timebitPublicKey, OPENSSL_ALGO_SHA256);
			if ($verify == 1) {
				return true;
			}
			return false;
		} catch (\Throwable $th) {
			return false;
		}
	}

	public function validateCost(CallbackState $state)
	{
		$purchaseRequest = $state->getPurchaseRequest();

		$currency = strtoupper($purchaseRequest->cost_currency);
		$cost = $purchaseRequest->cost_amount;

		$amountPaid = $state->amount;
		$currencyPaid = strtoupper($state->currency);
		if ($amountPaid == $cost && ($currencyPaid == $currency || ($currency == 'USD' && in_array($currencyPaid, ['USDT', 'TRX_USDT'])))) {
			return true;
		}

		$state->logType = 'error';
		$state->logMessage = 'Invalid cost amount';
		return false;
	}

	protected function getDataToVerifySignature(array $data = null)
	{
		if ($data === null) {
			$data = request()->except(['signature']);
		}

		ksort($data);

		$query = '';
		foreach ($data as $key => $value) {
			$query .= "{$key}={$value}&";
		}

		return $query;
	}

	public function getPaymentResult(CallbackState $state)
	{
		switch ($state->status) {
			case 'Completed':
				$state->paymentResult = CallbackState::PAYMENT_RECEIVED;
				break;
		}
	}

	public function prepareLogData(CallbackState $state)
	{
		$state->logDetails = request()->all();
	}

	protected $timebitApi;

	/**
	 * @return ApiTimebit
	 */
	protected function getTimebitApi()
	{
		if ($this->timebitApi === null) {
			$options = $this->getProviderOptions();
			$this->timebitApi = resolve(ApiTimebit::class, ['options' => $options]);
		}
		return $this->timebitApi;
	}
}
