<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;

class PaymentProfile extends AbstractModel
{
    protected $hidden = [
        'options',
    ];

    protected $casts = [
        'options' => 'json',
    ];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_payment_profile')->published();
    }

    public function providerId()
    {
        return $this->belongsTo(self::class);
    }

    public function providerIdList()
    {
        $providers = config('payments.providers', []);

        return array_map(function ($provider) {
            return Arr::access([
                'id' => $provider,
                'name' => paymentProvider($provider)->getTitle()
            ]);
        }, array_keys($providers));
    }

    protected $callFuns = [];
    public function getProviderHandlerAttribute()
    {
        $key = $this->getKey();
        if (isset($this->callFuns[__METHOD__][$key])) {
            return;
        }
        $this->callFuns[__METHOD__][$key] = true;

        return paymentProvider($this->provider_id);
    }

    public function scopeActive(Builder $query)
    {
        return $query->where('active', true);
    }

    public function scopeOrdered($query)
    {
        $query->orderBy('order', 'ASC');
    }

    public function prepareFilter(Builder $query, $field, $condition, $isNot = false)
    {
        switch ($field) {
            case 'id':
            case 'provider_id':
                $this->whereEqual($query, $field, $condition, $isNot);
                break;

            case 'active':
                $this->whereEqual($query, $field, (bool) $condition, $isNot);
                break;
        }
    }

    public function getFilterRules()
    {
        return [
            'products' => 'array,nullable',

            'id' => 'list-uint|uint|array-uint,nullable',
            'provider_id' => 'list-uint|uint|array-uint,nullable',
            'active' => 'bool,nullable',
        ];
    }
}
