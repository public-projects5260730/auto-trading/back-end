<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Broker extends AbstractModel
{
    use HasFactory;

    /**
     * Scope a query to only published scopes.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished(Builder $query)
    {
        return $query->where('active', '=', true);
    }

    public function scopeOrdered($query)
    {
        $query->orderBy('order', 'ASC');
    }

    public function prepareFilter(Builder $query, $field, $condition, $isNot = false)
    {
        switch ($field) {
            case 'id':
                $this->whereEqual($query, $field, $condition, $isNot);
                break;
        }
    }

    public function getFilterRules()
    {
        return [
            'id' => 'list-uint|uint|array-uint,nullable',
        ];
    }
}
