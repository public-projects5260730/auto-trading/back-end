<?php

namespace App\Models;

use App\Models\Traits\Trans;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use TCG\Voyager\Traits\Translatable;

class FaqGroup extends AbstractModel
{
    use HasFactory, Translatable, Trans;

    protected $translatable = ['title'];

    protected $appends = ['trans'];

    protected $hidden = [
        'translations',
    ];

    public function faqs()
    {
        return $this->belongsToMany(Faq::class, 'faq_group_faq')->ordered();
    }

    public function scopeOrdered($query)
    {
        $query->orderBy('order', 'ASC');
    }

    public function prepareFilter(Builder $query, $field, $condition, $isNot = false)
    {
        switch ($field) {
            case 'id':
                $this->whereEqual($query, $field, $condition, $isNot);
                break;

            case 'title':
                if (!is_array($condition)) {
                    $query->whereTranslation($field, strtolower($condition));
                } else {
                    $query->where(function ($query) use ($field, $condition) {
                        foreach ($condition as $_condition) {
                            $query->orWhere(function ($query) use ($field, $_condition) {
                                $query->whereTranslation($field, strtolower($_condition));
                            });
                        }
                    });
                }
                break;
        }
    }

    public function getFilterRules()
    {
        return [
            'faqs' => 'array,nullable',

            'id' => 'list-uint|uint|array-uint,nullable',
            'title' => 'str,nullable',
        ];
    }
}
