<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class PurchaseRequest extends AbstractModel
{
    public $timestamps = false;

    protected $casts = [
        'extra_data' => 'json',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function paymentProfile()
    {
        return $this->belongsTo(PaymentProfile::class);
    }

    public function prepareFilter(Builder $query, $field, $condition, $isNot = false)
    {
        switch ($field) {
            case 'id':
            case 'user_id':
            case 'payment_profile_id':

            case 'request_key':
            case 'provider_id':
            case 'purchasable_type_id':
            case 'cost_currency':
                $this->whereEqual($query, $field, $condition, $isNot);
                break;

            case 'cost_amount':
                $this->whereRange($query, $field, $condition, $isNot);
                break;
        }
    }

    public function getFilterRules()
    {
        return [
            'id' => 'list-uint|uint|array-uint,nullable',
            'user_id' => 'list-uint|uint|array-uint,nullable',
            'payment_profile_id' => 'list-uint|uint|array-uint,nullable',

            'request_key' => 'list-str|str|array-str,nullable',
            'provider_id' => 'list-str|str|array-str,nullable',
            'purchasable_type_id' => 'list-str|str|array-str,nullable',
            'cost_currency' => 'list-str|str|array-str,nullable',

            'cost_amount' => 'array,nullable',
        ];
    }
}
