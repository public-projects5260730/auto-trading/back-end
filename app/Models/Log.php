<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class Log extends AbstractModel
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'user_id',
        'table',
        'action',
        'message',
        'meta'
    ];

    public function setMetaAttribute($value)
    {
        if (!is_string($value)) {
            $value = json_encode($value);
        }
        $this->attributes['meta'] = $value;
    }
}
