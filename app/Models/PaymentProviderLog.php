<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;

class PaymentProviderLog extends AbstractModel
{
    protected $casts = [
        'log_details' => 'json',
    ];

    public function prepareFilter(Builder $query, $field, $condition, $isNot = false)
    {
        switch ($field) {
            case 'id':
            case 'purchase_request_key':
            case 'provider_id':
            case 'transaction_id':
            case 'subscriber_id':
            case 'log_type':
                $this->whereEqual($query, $field, $condition, $isNot);
                break;
        }
    }

    public function getFilterRules()
    {
        return [
            'id' => 'list-uint|uint|array-uint,nullable',
            'provider_id' => 'list-uint|uint|array-uint,nullable',

            'purchase_request_key' => 'list-str|str|array-str,nullable',
            'transaction_id' => 'list-str|str|array-str,nullable',
            'subscriber_id' => 'list-str|str|array-str,nullable',
            'log_type' => 'list-str|str|array-str,nullable',
        ];
    }
}
