<?php

namespace App\Models;

use App\Models\Traits\Trans;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use TCG\Voyager\Traits\Translatable;

class Faq extends AbstractModel
{
    use HasFactory, Translatable, Trans;

    protected $translatable = ['title', 'content'];

    protected $appends = ['trans'];

    protected $hidden = [
        'translations',
    ];

    public function groups()
    {
        return $this->belongsToMany(FaqGroup::class, 'faq_group_faq');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_faq')->published();
    }

    public function scopeOrdered($query)
    {
        $query->orderBy('order', 'ASC');
    }

    public function prepareFilter(Builder $query, $field, $condition, $isNot = false)
    {
        switch ($field) {
            case 'id':
                $this->whereEqual($query, $field, $condition, $isNot);
                break;

            case 'title':
            case 'content':
                if (!is_array($condition)) {
                    $query->whereTranslation($field, strtolower($condition));
                } else {
                    $query->where(function ($query) use ($field, $condition) {
                        foreach ($condition as $_condition) {
                            $query->orWhere(function ($query) use ($field, $_condition) {
                                $query->whereTranslation($field, strtolower($_condition));
                            });
                        }
                    });
                }
                break;
        }
    }

    public function getFilterRules()
    {
        return [
            'groups' => 'array,nullable',
            'products' => 'array,nullable',

            'id' => 'list-uint|uint|array-uint,nullable',
            'title' => 'str,nullable',
            'content' => 'str,nullable',
        ];
    }
}
