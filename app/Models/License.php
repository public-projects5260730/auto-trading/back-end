<?php

namespace App\Models;

use App\Models\Traits\Trans;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use TCG\Voyager\Traits\Translatable;

class License extends AbstractModel
{
    use HasFactory, Translatable, Trans;

    protected $translatable = ['title'];

    protected $appends = ['trans'];

    protected $hidden = [
        'translations',
    ];

    public function scopeActive(Builder $query)
    {
        return $query->where('active', true);
    }

    public function scopeOrdered($query)
    {
        $query->orderBy('order', 'ASC');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_license')->published();
    }

    public function prepareFilter(Builder $query, $field, $condition, $isNot = false)
    {
        switch ($field) {
            case 'id':
            case 'cost_currency':
                $this->whereEqual($query, $field, $condition, $isNot);
                break;

            case 'title':
                $this->whereTranslation($field, 'LIKE', '%' . $condition . '%');
                break;

            case 'is_trial':
            case 'active':
                $this->whereEqual($query, $field, (bool) $condition, $isNot);
                break;

            case 'account_number':
            case 'license_days':
            case 'cost_amount':
                $this->whereRange($query, $field, $condition, $isNot);
                break;
        }
    }

    public function getFilterRules()
    {
        return [
            'products' => 'array,nullable',

            'id' => 'list-uint|uint|array-uint,nullable',
            'cost_currency' => 'list-str|str|array-str,nullable',
            'title' => 'str,nullable',
            'is_trial' => 'bool,nullable',
            'active' => 'bool,nullable',
            'account_number' => 'array,nullable',
            'license_days' => 'array,nullable',
            'cost_amount' => 'array,nullable',
        ];
    }
}
