<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class LiveTransaction extends AbstractModel
{
    use HasFactory;

    protected $casts = [
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    public function accountLive()
    {
        return $this->belongsTo(AccountLive::class);
    }

    public function scopePublished(Builder $query)
    {
        return $query->where('order_type', '<', 6);
    }

    public function prepareFilter(Builder $query, $field, $condition, $isNot = false)
    {
        switch ($field) {
            case 'id':
            case 'symbol':
                $this->whereEqual($query, $field, $condition, $isNot);
                break;

            case 'close_price':
            case 'close_time':
            case 'commission':
            case 'created_at':
            case 'duration':
            case 'lot':
            case 'open_price':
            case 'open_time':
            case 'order_type':
            case 'pips':
            case 'profit_gross':
            case 'profit_net':
            case 'sl_price':
            case 'swap':
            case 'ticket':
            case 'tp_price':
            case 'updated_at':
            case 'account_live_id':
                $this->whereRange($query, $field, $condition, $isNot);
                break;

            case 'fromDate':
                if (!empty($condition) && is_numeric($condition)) {
                    $query->where('close_time', '>=', $condition);
                }
                break;

            case 'toDate':
                if (!empty($condition) && is_numeric($condition)) {
                    $query->where('close_time', '<=', $condition);
                }
                break;
        }
    }

    public function getFilterRules()
    {
        return [
            'accountLive' => 'array,nullable',

            'id' => 'list-uint|uint|array-uint,nullable',
            'symbol' => 'list-str|str|array-str,nullable',

            'close_price' => 'array,nullable',
            'close_time' => 'array,nullable',
            'commission' => 'array,nullable',
            'created_at' => 'array,nullable',
            'duration' => 'array,nullable',
            'lot' => 'array,nullable',
            'open_price' => 'array,nullable',
            'open_time' => 'array,nullable',
            'order_type' => 'array,nullable',
            'pips' => 'array,nullable',
            'profit_gross' => 'array,nullable',
            'profit_net' => 'array,nullable',
            'sl_price' => 'array,nullable',
            'swap' => 'array,nullable',
            'ticket' => 'array,nullable',
            'tp_price' => 'array,nullable',
            'updated_at' => 'array,nullable',
            'account_live_id' => 'array,nullable',

            'fromDate' => 'uint,nullable',
            'toDate' => 'uint,nullable',
        ];
    }

    protected function isValidOrder($order)
    {
        return in_array($order, [
            'id',
            'symbol',
            'close_price',
            'close_time',
            'commission',
            'created_at',
            'duration',
            'lot',
            'open_price',
            'open_time',
            'order_type',
            'pips',
            'profit_gross',
            'profit_net',
            'sl_price',
            'swap',
            'ticket',
            'tp_price',
            'updated_at',
            'account_live_id',
        ]);
    }
}
