<?php

namespace App\Models;

use App\Models\Traits\Trans;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use TCG\Voyager\Traits\Translatable;

class Category extends AbstractModel
{
    use HasFactory, Translatable, Trans;

    protected $translatable = ['name'];

    protected $fillable = ['name'];

    protected $appends = ['trans'];

    protected $hidden = [
        'translations',
    ];

    public function products()
    {
        return $this->hasMany(Product::class)
            ->published()
            ->casts()
            ->orderBy('order', 'ASC');
    }

    public function parentId()
    {
        return $this->belongsTo(self::class);
    }

    public function scopeOrdered($query)
    {
        $query->orderBy('order', 'ASC');
    }

    public function prepareFilter(Builder $query, $field, $condition, $isNot = false)
    {
        switch ($field) {
            case 'id':
            case 'slug':
                $this->whereEqual($query, $field, $condition, $isNot);
                break;

            case 'name':
                if (!is_array($condition)) {
                    $query->whereTranslation($field, strtolower($condition));
                } else {
                    $query->where(function ($query) use ($field, $condition) {
                        foreach ($condition as $_condition) {
                            $query->orWhere(function ($query) use ($field, $_condition) {
                                $query->whereTranslation($field, strtolower($_condition));
                            });
                        }
                    });
                }
                break;
        }
    }

    public function getFilterRules()
    {
        return [
            'products' => 'array,nullable',

            'id' => 'list-uint|uint|array-uint,nullable',
            'slug' => 'list-str|str|array-str,nullable',
            'name' => 'list-str|str|array-str,nullable',
        ];
    }
}
