<?php

namespace App\Models;

class AsyncBotLicense extends AbstractModel
{
    protected $casts = [
        'data' => 'json',
    ];
}
