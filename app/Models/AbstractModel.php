<?php

namespace App\Models;

use App\Models\Traits\Casts;
use App\Models\Traits\Filters;
use App\Models\Traits\Orders;
use App\Models\Traits\SetAttributes;
use Illuminate\Database\Eloquent\Model;

abstract class AbstractModel extends Model
{
    use SetAttributes, Filters, Orders, Casts;
}
