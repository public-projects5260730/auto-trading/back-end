<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Builder;

trait Orders
{
    public function scopeOrders(Builder $query, array $orders = null)
    {
        if ($orders === null) {
            $orders = request()->get('orders');
        }

        if (!is_array($orders) || empty($orders)) {
            return;
        }

        foreach ($orders as $order => $direction) {
            if (is_numeric($order) && is_array($direction)) {
                $query->orders($direction);
                continue;
            }

            if (is_string($order)) {
                $this->prepareOrder($query, $order, is_string($direction) ? $direction : 'ASC');
            }
        }
    }

    protected function prepareOrder(Builder $query, $field, $direction = null)
    {
        if (!$this->isValidOrder($field)) {
            return;
        }

        $query->orderBy($field, strtolower($direction) == 'desc' ? 'DESC' : 'ASC');
    }

    protected function isValidOrder($order)
    {
        return false;
    }
}
