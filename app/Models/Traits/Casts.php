<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Builder;

trait Casts
{
    public function scopeCasts(Builder $query, array $casts = null)
    {
        if ($casts === null) {
            $casts = [
                'created_at' => 'timestamp',
                'updated_at' => 'timestamp',
            ];
        }
        $query->withCasts($casts);
    }
}
