<?php

namespace App\Models\Traits;

use Illuminate\Support\Arr;

trait SetAttributes
{
    public function setAttributes(array $attributes, $overwrite = false)
    {
        foreach ($attributes as $key => $value) {
            if ($overwrite || Arr::get($this->attributes, $key) === null) {
                Arr::set($this->attributes, $key, $value);
            }
        }
    }
}
