<?php

namespace App\Models\Traits;

use Illuminate\Support\Arr;

trait Trans
{
    protected $callFuns = [];
    public function getTransAttribute()
    {
        $key = $this->getKey();
        if (isset($this->callFuns[__METHOD__][$key])) {
            return;
        }
        $this->callFuns[__METHOD__][$key] = true;

        $trans = [];
        $multilingual = config('voyager.multilingual');
        if (empty($multilingual['enabled'])) {
            return $trans;
        }

        $defaultLanguage = $multilingual['default'];
        $trans[$defaultLanguage] = [
            'language' => $defaultLanguage
        ] + $this->attributesToArray();

        if (Arr::hasAny($this->getRelations(), 'translations') && !empty($multilingual['locales'])) {
            foreach ($this->translations as $translation) {
                $locale = $translation->locale;
                if (!isset($trans[$locale])) {
                    $trans[$locale] = $trans[$defaultLanguage];
                    $trans[$locale]['language'] = $locale;
                }
                $trans[$locale][$translation->column_name] = $translation->value;
            }
        }
        return array_values($trans);
    }
}
