<?php

namespace App\Models\Traits;

use Illuminate\Database\Eloquent\Builder;

trait Filters
{
    public function scopeFilters($query, array $filters = null, $isNot = false)
    {
        $filters = $this->getFilters($filters);

        foreach ($filters as $field => $condition) {
            if (is_numeric($field) && is_array($condition)) {
                $query->filters($condition);
                continue;
            }

            if ($this->relationFilters($query, $field, $condition)) {
                continue;
            }

            switch (strval($field)) {
                case '__or':
                    if (!empty($condition) && is_array($condition)) {
                        $query->where(function ($query) use ($condition) {
                            foreach ($condition as $field => $_condition) {
                                $query->orWhere(function ($query) use ($field, $_condition) {
                                    $query->filters([$field => $_condition]);
                                });
                            }
                        });
                    }
                    break;

                case '__not':
                    if (!empty($condition) && is_array($condition)) {
                        $query->filters($condition, true);
                    }
                    break;

                default:
                    $this->prepareFilter($query, $field, $condition, $isNot);
            }
        }
    }

    protected function relationFilters(Builder $query, $relation, $conditions)
    {
        if (!is_array($conditions) || !$this->isRelation($relation)) {
            return;
        }

        $query->whereHas($relation, function (Builder $query) use ($conditions) {
            if ($query->hasNamedScope('filters')) {
                $query->filters($conditions);
            }
        });

        return true;
    }

    protected function prepareFilter(Builder $query, $field, $condition, $isNot = false)
    {
    }

    protected function getFilters(array $filters = null)
    {
        $rules = $this->getFilterRules();
        $rules += [
            '__or' => 'array,nullable',
            '__not' => 'array,nullable',
        ];

        if (empty($rules) || !is_array($rules)) {
            return [];
        }

        if ($filters === null) {
            $filters = request()->only(array_keys($rules));
        }

        return filter($filters, $rules);
    }

    protected function getFilterRules()
    {
        return [];
    }

    protected function whereEqual(Builder $query, string $field, $condition, $isNot = false)
    {
        if ($isNot) {
            if (!is_array($condition)) {
                $query->where($field, '!=', $condition);
            } else {
                $query->whereNotIn($field, $condition);
            }
        } else {
            if (!is_array($condition)) {
                $query->where($field, $condition);
            } else {
                $query->whereIn($field, $condition);
            }
        }
    }

    protected function whereLike(Builder $query, string $field, $condition, $isNot = false)
    {
        if ($isNot) {
            $query->where($field, 'NOT LIKE', $condition);
        } else {
            $query->where($field, 'LIKE', $condition);
        }
    }

    protected function whereRange(Builder $query, string $field, $condition)
    {
        list($operator, $cutOff) = $condition;
        $query->where($field, $operator, $cutOff);
    }
}
