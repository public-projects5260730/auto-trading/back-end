<?php

namespace App\Models;

use App\Models\Traits\Trans;
use App\Utils\Constants;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Query\Builder as QueryBuilder;
use TCG\Voyager\Traits\Resizable;
use TCG\Voyager\Traits\Translatable;

class Product extends AbstractModel
{
    use HasFactory, Resizable, Translatable, Trans;

    protected $translatable = ['excerpt', 'overview', 'comments', 'versions', 'guide'];

    protected $appends = ['trans'];

    protected $hidden = [
        'translations',
        'files',
    ];

    public function accountLive()
    {
        return $this->belongsTo(AccountLive::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function licenses()
    {
        return $this->belongsToMany(License::class, 'product_license')->active()->ordered();
    }

    public function paymentProfiles()
    {
        return $this->belongsToMany(PaymentProfile::class, 'product_payment_profile')->active()->ordered();
    }

    public function getBotFilesAttribute()
    {
        $files = $this->files;
        if (empty($files)) {
            return [];
        }

        if (is_array($files)) {
            return $files;
        }
        return json_decode($files, true);
    }


    /**
     * Scope a query to only published scopes.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePublished(Builder $query)
    {
        return $query->where('status', '=', Constants::PRODUCT_STATUS['PUBLISHED']);
    }

    public function scopeOrdered($query)
    {
        $query->orderBy('order', 'ASC');
    }

    protected function prepareOrder(Builder $query, $field, $direction = null)
    {
        $this->joinOrder($query, $field, $direction);
        return parent::prepareOrder($query, $field, $direction);
    }

    private $isJoinStats;
    protected function joinOrder(Builder $query, $field, $direction = null)
    {
        if ($this->isValidOrder($field) || !in_array($field, $this->getStartOrders())) {
            return;
        }

        if (!$this->isJoinStats) {
            $this->isJoinStats = true;

            /** @var QueryBuilder $query */
            $query->leftJoin('account_live', 'account_live.id', '=', 'products.account_live_id')
                ->leftJoin('account_live_stats', 'account_live_stats.account_live_id', '=', 'account_live.id');
        }

        $query->orderBy("account_live_stats.{$field}", strtolower($direction) == 'desc' ? 'DESC' : 'ASC');
    }

    public function prepareFilter(Builder $query, $field, $condition, $isNot = false)
    {
        switch ($field) {
            case 'id':
            case 'slug':
            case 'status':
                $this->whereEqual($query, $field, $condition, $isNot);
                break;

            case 'title':
                $this->whereLike($query, $field, '%' . $condition . '%', $isNot);
                break;

            case 'search':
                $this->whereLike($query, 'title', '%' . $condition . '%', $isNot);
                break;
                break;

            case 'featured':
                $this->whereEqual($query, $field, (bool) $condition, $isNot);
                break;

            case 'searchLanguage':
                $query->where(function ($query) use ($condition) {
                    $fields = ['excerpt', 'overview', 'comparison', 'guide'];
                    foreach ($fields as $field) {
                        $query->orWhere(function ($query) use ($field, $condition) {
                            $query->whereTranslation($field, 'LIKE', '%' . $condition . '%');
                        });
                    }
                });
        }
    }

    public function getFilterRules()
    {
        return [
            'category' => 'array,nullable',
            'accountLive' => 'array,nullable',

            'id' => 'list-uint|uint|array-uint,nullable',
            'status' => 'list-uint|uint|array-uint,nullable',
            'slug' => 'list-str|str|array-str,nullable',
            'featured' => 'bool,nullable',
            'title' => 'str,nullable',
            'search' => 'str,nullable',
        ];
    }

    protected function getStartOrders()
    {
        return [
            'avg_loss_trade',
            'avg_profit_trade',
            'avg_time_trade',
            'balance',
            'best_pips',
            'best_profit',
            'buy_trades',
            'buy_win_rate',
            'buy_win_trades',
            'commission_total',
            'created_at',
            'credit',
            'deposit_init',
            'deposit_total',
            'equity',
            'loss_rate',
            'loss_trades',
            'lot_max',
            'lot_min',
            'lot_total',
            'max_drawdown',
            'pips_total',
            'profit_factor',
            'profit_net',
            'profit_rate',
            'profit_running',
            'sell_trades',
            'sell_win_rate',
            'sell_win_trades',
            'start_time_trade',
            'swap_total',
            'time_best_pips',
            'time_best_profit',
            'time_trade_total',
            'time_worst_pips',
            'time_worst_profit',
            'total_trades',
            'updated_at',
            'win_rate',
            'win_trades',
            'withdrawals_total',
            'worst_pips',
            'worst_profit',
            'account_live_id',
        ];
    }
}
