<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AccountLiveStat extends AbstractModel
{
    use HasFactory;

    protected $casts = [
        'profit_rates_by_day' => 'json',
        'profit_by_day' => 'json',
        'symbol_collection' => 'json',
        'gain_collection' => 'json'
    ];

    public function accountLive()
    {
        return $this->belongsTo(AccountLive::class);
    }

    public function prepareFilter(Builder $query, $field, $condition, $isNot = false)
    {
        switch ($field) {
            case 'id':
                $this->whereEqual($query, $field, $condition, $isNot);
                break;

            case 'avg_loss_trade':
            case 'avg_profit_trade':
            case 'avg_time_trade':
            case 'balance':
            case 'best_pips':
            case 'best_profit':
            case 'buy_trades':
            case 'buy_win_rate':
            case 'buy_win_trades':
            case 'commission_total':
            case 'created_at':
            case 'credit':
            case 'deposit_init':
            case 'deposit_total':
            case 'equity':
            case 'loss_rate':
            case 'loss_trades':
            case 'lot_max':
            case 'lot_min':
            case 'lot_total':
            case 'max_drawdown':
            case 'pips_total':
            case 'profit_factor':
            case 'profit_net':
            case 'profit_rate':
            case 'profit_running':
            case 'sell_trades':
            case 'sell_win_rate':
            case 'sell_win_trades':
            case 'start_time_trade':
            case 'swap_total':
            case 'time_best_pips':
            case 'time_best_profit':
            case 'time_trade_total':
            case 'time_worst_pips':
            case 'time_worst_profit':
            case 'total_trades':
            case 'updated_at':
            case 'win_rate':
            case 'win_trades':
            case 'withdrawals_total':
            case 'worst_pips':
            case 'worst_profit':
            case 'account_live_id':
                $this->whereRange($query, $field, $condition, $isNot);
                break;
        }
    }

    public function getFilterRules()
    {
        return [
            'accountLive' => 'array,nullable',

            'id' => 'list-uint|uint|array-uint,nullable',

            'avg_loss_trade' => 'array,nullable',
            'avg_profit_trade' => 'array,nullable',
            'avg_time_trade' => 'array,nullable',
            'balance' => 'array,nullable',
            'best_pips' => 'array,nullable',
            'best_profit' => 'array,nullable',
            'buy_trades' => 'array,nullable',
            'buy_win_rate' => 'array,nullable',
            'buy_win_trades' => 'array,nullable',
            'commission_total' => 'array,nullable',
            'created_at' => 'array,nullable',
            'credit' => 'array,nullable',
            'deposit_init' => 'array,nullable',
            'deposit_total' => 'array,nullable',
            'equity' => 'array,nullable',
            'loss_rate' => 'array,nullable',
            'loss_trades' => 'array,nullable',
            'lot_max' => 'array,nullable',
            'lot_min' => 'array,nullable',
            'lot_total' => 'array,nullable',
            'max_drawdown' => 'array,nullable',
            'pips_total' => 'array,nullable',
            'profit_factor' => 'array,nullable',
            'profit_net' => 'array,nullable',
            'profit_rate' => 'array,nullable',
            'profit_running' => 'array,nullable',
            'sell_trades' => 'array,nullable',
            'sell_win_rate' => 'array,nullable',
            'sell_win_trades' => 'array,nullable',
            'start_time_trade' => 'array,nullable',
            'swap_total' => 'array,nullable',
            'time_best_pips' => 'array,nullable',
            'time_best_profit' => 'array,nullable',
            'time_trade_total' => 'array,nullable',
            'time_worst_pips' => 'array,nullable',
            'time_worst_profit' => 'array,nullable',
            'total_trades' => 'array,nullable',
            'updated_at' => 'array,nullable',
            'win_rate' => 'array,nullable',
            'win_trades' => 'array,nullable',
            'withdrawals_total' => 'array,nullable',
            'worst_pips' => 'array,nullable',
            'worst_profit' => 'array,nullable',
            'account_live_id' => 'array,nullable',
        ];
    }

    protected function isValidOrder($order)
    {
        return in_array($order, [
            'avg_loss_trade',
            'avg_profit_trade',
            'avg_time_trade',
            'balance',
            'best_pips',
            'best_profit',
            'buy_trades',
            'buy_win_rate',
            'buy_win_trades',
            'commission_total',
            'created_at',
            'credit',
            'deposit_init',
            'deposit_total',
            'equity',
            'loss_rate',
            'loss_trades',
            'lot_max',
            'lot_min',
            'lot_total',
            'max_drawdown',
            'pips_total',
            'profit_factor',
            'profit_net',
            'profit_rate',
            'profit_running',
            'sell_trades',
            'sell_win_rate',
            'sell_win_trades',
            'start_time_trade',
            'swap_total',
            'time_best_pips',
            'time_best_profit',
            'time_trade_total',
            'time_worst_pips',
            'time_worst_profit',
            'total_trades',
            'updated_at',
            'win_rate',
            'win_trades',
            'withdrawals_total',
            'worst_pips',
            'worst_profit',
            'account_live_id',
        ]);
    }
}
