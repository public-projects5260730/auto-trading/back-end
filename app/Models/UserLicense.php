<?php

namespace App\Models;

use App\Utils\Constants;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Gate;

class UserLicense extends AbstractModel
{
    protected $casts = [
        'activated_accounts' => 'json',
        'extra' => 'json',
        'upgrade' => 'json',
        'bot_response' => 'json',
    ];

    protected $appends = ['permissions'];

    protected $hidden = [
        'extra',
        'upgrade',
        'bot_response',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function license()
    {
        return $this->belongsTo(License::class);
    }

    public function getPermissionsAttribute()
    {
        /** @var User $user */
        $user = auth()->user();
        $userLicense = $this;

        return [
            'canDownload' => $user ? $user->can('download', $userLicense) : Gate::allows('download', $userLicense),
            'canExtend' => $user ? $user->can('extend', $userLicense) : false,
            'canUpgrade' => $user ? $user->can('upgrade', $userLicense) : false,
        ];
    }

    public function scopeActive(Builder $query)
    {
        return $query->whereIn('state', Arr::only(Constants::USER_LICENSE_STATES, 'NEW', 'ACTIVATED'));
    }

    public function scopeOwner(Builder $query)
    {
        return $query->where('user_id', auth()->id() ?? -1);
    }

    public function scopeOrdered($query)
    {
        $query->orderBy('created_at', 'DESC');
    }

    public function prepareFilter(Builder $query, $field, $condition, $isNot = false)
    {
        switch ($field) {
            case 'id':
            case 'user_id':
            case 'product_id':
            case 'license_id':
            case 'state':

            case 'purchase_request_key':
                $this->whereEqual($query, $field, $condition, $isNot);
                break;

            case 'cost_amount':
                $this->whereRange($query, $field, $condition, $isNot);
                break;

            case 'is_trial':
            case 'is_synced':
                $this->whereEqual($query, $field, (bool) $condition, $isNot);
                break;
        }
    }

    public function getFilterRules()
    {
        return [
            'user' => 'array,nullable',
            'product' => 'array,nullable',
            'license' => 'array,nullable',

            'id' => 'list-uint|uint|array-uint,nullable',
            'user_id' => 'list-uint|uint|array-uint,nullable',
            'product_id' => 'list-uint|uint|array-uint,nullable',
            'license_id' => 'list-uint|uint|array-uint,nullable',
            'state' => 'list-uint|uint|array-uint,nullable',

            'purchase_request_key' => 'list-str|str|array-str,nullable',

            'is_trial' => 'bool,nullable',
            'is_synced' => 'bool,nullable',
        ];
    }
}
