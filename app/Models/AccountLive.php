<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AccountLive extends AbstractModel
{
    use HasFactory;

    protected $table = 'account_live';

    public function stats()
    {
        return $this->hasOne(AccountLiveStat::class);
    }

    public function product()
    {
        return $this->hasOne(Product::class)->published()->casts();
    }

    public function prepareFilter(Builder $query, $field, $condition, $isNot = false)
    {
        switch ($field) {
            case 'id':
            case 'account_id':
            case 'currency':
                $this->whereEqual($query, $field, $condition, $isNot);
                break;

            case 'account_name':
            case 'leverage':
            case 'server_name':
                $this->whereLike($query, $field, "%{$condition}%", $isNot);
                break;

            case 'created_at':
            case 'updated_at':
                $this->whereRange($query, $field, $condition, $isNot);
                break;
        }
    }

    public function getFilterRules()
    {
        return [
            'product' => 'array,nullable',
            'stats' => 'array,nullable',

            'id' => 'list-uint|uint|array-uint,nullable',
            'account_id' => 'list-uint|uint|array-uint,nullable',
            'currency' => 'list-str|str|array-str,nullable',
            'account_name' => 'str,nullable',
            'leverage' => 'str,nullable',
            'server_name' => 'str,nullable',

            'created_at' => 'array,nullable',
            'updated_at' => 'array,nullable',
        ];
    }
}
