<?php

namespace App\Models;

use App\Models\Traits\Trans;
use Illuminate\Database\Eloquent\Builder;
use TCG\Voyager\Traits\Translatable;

class Page extends AbstractModel
{
    use Translatable, Trans;

    protected $translatable = ['body'];

    protected $appends = ['trans'];

    protected $hidden = [
        'translations',
    ];

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function prepareFilter(Builder $query, $field, $condition, $isNot = false)
    {
        switch ($field) {
            case 'id':
            case 'slug':
                $this->whereEqual($query, $field, $condition, $isNot);
                break;

            case 'title':
                $this->whereLike($query, $field, '%' . $condition . '%', $isNot);
                break;

            case 'active':
                $this->whereEqual($query, $field, (bool) $condition, $isNot);
                break;
        }
    }

    public function getFilterRules()
    {
        return [
            'id' => 'list-uint|uint|array-uint,nullable',
            'slug' => 'list-str|str|array-str,nullable',
            'title' => 'str,nullable',
            'active' => 'bool,nullable',
        ];
    }
}
