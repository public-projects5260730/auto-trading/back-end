<?php

namespace App\Utils;

class Constants
{
    const PER_PAGE = 20;

    const HTTP_TIMEOUT = 30;

    const LIMIT_MESSAGES_PER_DAY = 10;

    const LIMIT_TRIAL_LICENSE_PER_DAY = 3;

    const PRODUCT_STATUS = [
        'PUBLISHED' => 1,
        'DRAFT' => 2,
        'PENDING' => 3,
    ];

    const USER_LICENSE_STATES = [
        'NEW' => 1,
        'ACTIVATED' => 2,
        'EXPIRED' => 3,
        'CANCELED' => 4,
        'REVOKED' => 5,
    ];

    const BOT_LICENSE_STATES = [
        'NEW' => 0,
        'ACTIVATED' => 1,
        'EXPIRED' => 2,
        'CANCELED' => 3,
    ];
}//end class
