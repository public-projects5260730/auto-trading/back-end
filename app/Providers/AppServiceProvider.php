<?php

namespace App\Providers;

use App\Helpers\DataFilterer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadViewsFrom(public_path(), 'public');
        $this->loadViewsFrom(resource_path('views/admin'), 'admin');

        $this->app->singleton('DataFilterer', function () {
            return new DataFilterer();
        });

        $this->paymentsRegistration();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (!!env('USE_HTTPS', false)) {
            URL::forceScheme('https');
        }

        if (config('app.debug') && request('__sql')) {
            DB::listen(function ($query) {
                dump(['sql' => $query->sql, 'bindings' => implode('|', $query->bindings), 'time' => $query->time]);
            });
        }
    }

    protected function paymentsRegistration()
    {
        $providers = config('payments.providers');
        if (!empty($providers) && is_array($providers)) {
            foreach ($providers as $providerId => $class) {
                if (is_string($providerId) && class_exists($class)) {
                    $this->app->singleton("payments.providers.{$providerId}", function () use ($class, $providerId) {
                        return new $class($providerId);
                    });
                }
            }
        }

        $purchasable = config('payments.purchasable');
        if (!empty($purchasable) && is_array($purchasable)) {
            foreach ($purchasable as $type => $class) {
                if (is_string($type) && class_exists($class)) {
                    $this->app->singleton("payments.purchasable.{$type}", function () use ($class, $type) {
                        return new $class($type);
                    });
                }
            }
        }
    }
}
