<?php

namespace App\Providers;

use App\Models\UserLicense;
use App\Observers\UserLicenseObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'eloquent.saved:*' => [
            \App\Listeners\SavedLogger::class
        ],
        'eloquent.deleted:*' => [
            \App\Listeners\DeletedLogger::class
        ],

        \App\Events\Notifications\UserLicense\Purchased::class => [
            \App\Listeners\Notifications\UserLicense\Emails\Purchased::class,
        ],
        // \App\Events\Notifications\UserLicense\Upgrade::class => [
        //     \App\Listeners\Notifications\UserLicense\Emails\Upgrade::class,
        // ],
        // \App\Events\Notifications\UserLicense\Downgrade::class => [
        //     \App\Listeners\Notifications\UserLicense\Emails\Downgrade::class,
        // ],
        // \App\Events\Notifications\UserLicense\ExpirationSoon::class => [
        //     \App\Listeners\Notifications\UserLicense\Emails\ExpirationSoon::class,
        // ],
        // \App\Events\Notifications\UserLicense\Expired::class => [
        //     \App\Listeners\Notifications\UserLicense\Emails\Expired::class,
        // ],
        // \App\Events\Notifications\UserLicense\Canceled::class => [
        //     \App\Listeners\Notifications\UserLicense\Emails\Canceled::class,
        // ],
        // \App\Events\Notifications\UserLicense\Revoked::class => [
        //     \App\Listeners\Notifications\UserLicense\Emails\Revoked::class,
        // ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        UserLicense::observe(UserLicenseObserver::class);
    }
}
