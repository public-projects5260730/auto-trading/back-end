<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;
use Illuminate\Routing\RouteRegistrar;
use Illuminate\Support\Facades\File;
use Symfony\Component\Finder\SplFileInfo;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/';

    /**
     * The controller namespace for the application.
     *
     * When present, controller route declarations will automatically be prefixed with this namespace.
     *
     * @var string|null
     */
    protected $namespace = 'App\\Http\\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();

        $this->routes(function () {
            $this->useAdminRoutes();
            $this->useApiRoutes();
            $this->useEmailRoutes();
            $this->usePublicRoutes();
        });
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });
    }

    protected function useAdminRoutes()
    {
        $adminRoute = Route::prefix('admin')->middleware('web')
            ->namespace($this->namespace . '\\Admin');

        $adminRoute->group(base_path('routes/admin.php'));
        $this->addRoutesByPath($adminRoute, base_path('routes/admin'));
    }

    protected function useApiRoutes()
    {
        $apiRoute = Route::prefix('api')
            ->name('api.')
            ->middleware('api')
            ->namespace($this->namespace . '\\Api');

        $apiRoute->group(base_path('routes/api.php'));
        $this->addRoutesByPath($apiRoute, base_path('routes/api'));
    }

    protected function usePublicRoutes()
    {
        $publicRoute = Route::middleware('web')
            ->namespace($this->namespace . '\\Pub');

        $publicRoute->group(base_path('routes/web.php'));
        $this->addRoutesByPath($publicRoute, base_path('routes/pub'));
    }

    protected function useEmailRoutes()
    {
        $emailRoute = Route::prefix('emails')
            ->middleware('web')
            ->namespace($this->namespace . '\\Email');

        $emailRoute->group(base_path('routes/email.php'));
        $this->addRoutesByPath($emailRoute, base_path('routes/email'));
    }

    protected function addRoutesByPath(RouteRegistrar $route, string $path)
    {
        if (!File::isDirectory($path)) {
            return;
        }
        $files = File::allFiles($path);
        if (!empty($files)) {

            foreach ($files as $file) {
                /** @var SplFileInfo $file */
                if ($file->getExtension() != 'php') {
                    continue;
                }
                $route->group($file->getPathname());
            }
        }
    }
}
