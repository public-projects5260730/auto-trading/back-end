<?php

namespace App\Listeners;

use App\Models\Log;
use App\Services\Logger;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Model;

class SavedLogger implements ShouldQueue
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event, $models)
    {
        if (is_array($models)) {
            foreach ($models as $model) {
                if ($model instanceof Model) {
                    $this->logger($model);
                }
            }
        } else if ($models instanceof Model) {
            $this->logger($models);
        }
    }

    protected function logger(Model $model)
    {
        if ($model instanceof Log) {
            return;
        }

        /** @var Logger $loggerService */
        $loggerService = resolve(Logger::class);

        if ($model->wasRecentlyCreated) {
            $loggerService->logger($model, 'CREATED');
        } else if ($model->wasChanged()) {
            $loggerService->logger($model, 'UPDATED');
        }
    }
}
