<?php

namespace App\Listeners\Notifications\UserLicense\Emails;

use App\Events\Notifications\UserLicense\Canceled as UserLicenseCanceled;

class Canceled
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserLicenseCanceled $event)
    {
        $userLicense = $event->userLicense;

        /** @var User $user */
        $user = $event->user;

        // $user->notify(new EmailsCanceled($userLicense));
    }
}
