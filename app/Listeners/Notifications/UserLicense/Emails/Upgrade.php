<?php

namespace App\Listeners\Notifications\UserLicense\Emails;

use App\Events\Notifications\UserLicense\Upgrade as UserLicenseUpgrade;

class Upgrade
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserLicenseUpgrade $event)
    {
        $userLicense = $event->userLicense;

        /** @var User $user */
        $user = $event->user;

        // $user->notify(new EmailsRevoked($userLicense));
    }
}
