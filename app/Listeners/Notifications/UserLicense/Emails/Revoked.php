<?php

namespace App\Listeners\Notifications\UserLicense\Emails;

use App\Events\Notifications\UserLicense\Revoked as UserLicenseRevoked;

class Revoked
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserLicenseRevoked $event)
    {
        $userLicense = $event->userLicense;

        /** @var User $user */
        $user = $event->user;

        // $user->notify(new EmailsRevoked($userLicense));
    }
}
