<?php

namespace App\Listeners\Notifications\UserLicense\Emails;

use App\Events\Notifications\UserLicense\Expired as UserLicenseExpired;

class Expired
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserLicenseExpired $event)
    {
        $userLicense = $event->userLicense;

        /** @var User $user */
        $user = $event->user;

        // $user->notify(new EmailsExpired($userLicense));
    }
}
