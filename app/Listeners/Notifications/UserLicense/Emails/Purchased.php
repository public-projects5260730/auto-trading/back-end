<?php

namespace App\Listeners\Notifications\UserLicense\Emails;

use App\Events\Notifications\UserLicense\Purchased as UserLicensePurchased;
use App\Models\User;
use App\Notifications\UserLicense\Emails\Purchased as EmailsPurchased;

class Purchased
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserLicensePurchased $event)
    {
        $userLicense = $event->userLicense;

        if (!env('NOTIFICATIONS_TRIAL_LICENSE', false) && $userLicense->is_trial) {
            return;
        }

        /** @var User $user */
        $user = $event->user;

        if (!$user) {
            $email = $userLicense->email;
            if (empty($email)) {
                return;
            }

            $user = User::make(['email' => $email, 'name' => $email]);
        }
        $user->notify(new EmailsPurchased($userLicense));
    }
}
