<?php

namespace App\Listeners\Notifications\UserLicense\Emails;

use App\Events\Notifications\UserLicense\ExpirationSoon as UserLicenseExpirationSoon;

class ExpirationSoon
{

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(UserLicenseExpirationSoon $event)
    {
        $userLicense = $event->userLicense;

        /** @var User $user */
        $user = $event->user;

        // $user->notify(new EmailsExpirationSoon($userLicense));
    }
}
