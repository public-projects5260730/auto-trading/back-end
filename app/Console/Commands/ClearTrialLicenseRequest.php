<?php

namespace App\Console\Commands;

use App\Models\TrialLicenseRequest;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class ClearTrialLicenseRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:clear-trial-license-request';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Flush expired trial license request';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $query = query(TrialLicenseRequest::class);
        $query->where('created_at', '<', Carbon::today())->delete();

        return Command::SUCCESS;
    }
}
