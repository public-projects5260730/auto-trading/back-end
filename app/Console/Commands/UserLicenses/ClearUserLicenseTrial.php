<?php

namespace App\Console\Commands\UserLicenses;

use App\Jobs\UserLicenses\ClearTrialLicense;
use App\Models\UserLicense;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class ClearUserLicenseTrial extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:user-licenses:clear-trial-license';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Flush expired trial license';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $expiredDate = Carbon::today()->addDays(- (max(1, config('licenses.trial.license_days',  1) + 3)));

        $query = query(UserLicense::class);
        $query->where('license_id', 0);
        $query->where('is_trial', 1);

        $query->where(function ($query) use ($expiredDate) {
            $query->orWhereNull('activated_at');
            $query->orWhereDate('activated_at', '<', $expiredDate);
        });

        $query->where(function ($query) {
            $query->orWhereNull('expired_at');
            $query->orWhereDate('expired_at', '<', Carbon::today());
        });

        $query->whereDate('created_at', '<', $expiredDate);

        $userLicenses = $query->get();

        if ($userLicenses && $userLicenses->count()) {
            $userLicenses->each(function ($userLicense) {
                ClearTrialLicense::dispatch($userLicense);
            });
        }

        return Command::SUCCESS;
    }
}
