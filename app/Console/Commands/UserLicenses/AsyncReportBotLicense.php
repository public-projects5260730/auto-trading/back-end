<?php

namespace App\Console\Commands\UserLicenses;

use App\Jobs\UserLicenses\AsyncReportBotLicense as UserLicensesAsyncReportBotLicense;
use App\Models\UserLicense;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;

class AsyncReportBotLicense extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:bot-licenses:async-report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Asynchronous report Bot Licenses with User Licenses';


    protected $batch = 100;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /** @var Collection $userLicenses */
        $userLicenses = UserLicense::query()->active()->where('is_synced', true)->get();

        if ($userLicenses && $userLicenses->count()) {
            $userLicenses->chunk($this->batch)->each(function ($userLicenses) {
                UserLicensesAsyncReportBotLicense::dispatch($userLicenses);
            });
        }

        return Command::SUCCESS;
    }
}
