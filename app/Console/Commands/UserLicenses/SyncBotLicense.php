<?php

namespace App\Console\Commands\UserLicenses;

use App\Jobs\UserLicenses\SyncBotLicense as UserLicensesSyncBotLicense;
use App\Models\UserLicense;
use Illuminate\Console\Command;

class SyncBotLicense extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:bot-licenses:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize User Licenses to Bot Licenses';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $userLicenses = UserLicense::query()->where('is_synced', false)->get();

        if ($userLicenses && $userLicenses->count()) {
            $userLicenses->each(function ($userLicense) {
                UserLicensesSyncBotLicense::dispatch($userLicense);
            });
        }

        return Command::SUCCESS;
    }
}
