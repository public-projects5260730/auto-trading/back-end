<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $language = config('voyager.multilingual.default', 'en');
        $locale = $request->header('Language', $language);
        $request->setLocale($locale);
        App::setLocale($locale);

        return $next($request);
    }
}
