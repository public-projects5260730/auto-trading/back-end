<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Response;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Facades\JWTAuth as FacadesJWTAuth;

class JwtAuth extends Middleware
{

    /**
     * Handle an unauthenticated user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  array  $guards
     * @return void
     *
     * @throws \Illuminate\Auth\AuthenticationException
     */
    protected function unauthenticated($request, array $guards)
    {
        try {
            FacadesJWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            if ($e instanceof TokenBlacklistedException) {
                throw new \Exception($e->getMessage(), Response::HTTP_LOCKED);
            } elseif ($e instanceof TokenExpiredException) {
                throw new \Exception($e->getMessage(), Response::HTTP_UNAUTHORIZED);
            } elseif ($e instanceof TokenInvalidException) {
                throw new \Exception(__('auth.token_invalid'), Response::HTTP_UNAUTHORIZED);
            }
            throw new \Exception(__('auth.token_not_found'), Response::HTTP_NETWORK_AUTHENTICATION_REQUIRED);
        }
    }
}
