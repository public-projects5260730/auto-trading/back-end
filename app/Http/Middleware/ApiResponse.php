<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ApiResponse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);

        if ($response instanceof JsonResponse) {
            $responseData = $response->getData(true);
            $responseData = array_replace($responseData, [
                'error' => $this->getError($responseData),
                'errors' => $this->getErrors($responseData),
            ]);
            $response->setData(Arr::whereNotNull($responseData));
        }
        return $response;
    }

    protected function getError(array $responseData)
    {
        $error = [];
        foreach (['error', 'errors'] as $key) {
            if (!empty($responseData[$key])) {
                if (!is_array($responseData[$key]) || Arr::isList($responseData[$key])) {
                    $error[] = Arr::errorJoin($responseData[$key]);
                }
            }
        }
        return Arr::errorJoin($error);
    }

    protected function getErrors(array $responseData)
    {
        $errors = [];
        foreach (['error', 'errors'] as $key) {
            if (!empty($responseData[$key])) {
                if (is_array($responseData[$key]) && Arr::isAssoc($responseData[$key])) {
                    $errors += $responseData[$key];
                }
            }
        }
        if (empty($errors)) {
            return;
        }
        foreach ($errors as &$error) {
            $error = Arr::errorJoin($error);
        }
        return Arr::whereNotNull($errors);
    }
}
