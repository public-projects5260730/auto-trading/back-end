<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class ProductController extends VoyagerBaseController
{
    public function create(Request $request)
    {
        $response = parent::create($request);

        if (view()->exists("admin.products.edit-add")) {
            return Voyager::view('admin.products.edit-add', $response->getData());
        }

        return $response;
    }

    public function edit(Request $request, $id)
    {
        $response = parent::edit($request, $id);

        if (view()->exists("admin.products.edit-add")) {
            return Voyager::view('admin.products.edit-add', $response->getData());
        }

        return $response;
    }
}
