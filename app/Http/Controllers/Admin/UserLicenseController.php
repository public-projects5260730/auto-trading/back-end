<?php

namespace App\Http\Controllers\Admin;

use App\Events\Notifications\UserLicense\Purchased;
use App\Models\UserLicense;
use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class UserLicenseController extends VoyagerBaseController
{
    public function emailPurchased(Request $request, $id)
    {
        $userLicense = query(UserLicense::class)->findOrFail($id);

        event(new Purchased($userLicense));

        return redirect('admin/user-licenses');
    }
}
