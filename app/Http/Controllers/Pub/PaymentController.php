<?php

namespace App\Http\Controllers\Pub;

use App\Http\Controllers\Controller;

class PaymentController extends Controller
{
    public function callback(string $providerId)
    {
        $handler = paymentProvider($providerId);
        $state = $handler->setupCallback();

        $statusCode = 0;
        if (!$handler->validateCallback($state)) {
            $statusCode = $state->httpCode ?: 403;
        } else if (!$handler->validateTransaction($state)) {
            $statusCode = $state->httpCode ?: 200;
        } else if (
            !$handler->validatePurchaseRequest($state)
            || !$handler->validatePurchasableHandler($state)
            || !$handler->validatePaymentProfile($state)
            || !$handler->validatePurchaser($state)
        ) {
            $statusCode = $state->httpCode ?: 404;
        } else if (
            !$handler->validatePurchasableData($state)
            || !$handler->validateCost($state)
        ) {
            $statusCode = $state->httpCode ?: 403;
        } else {
            $handler->setProviderMetadata($state);
            $handler->getPaymentResult($state);
            $handler->completeTransaction($state);
        }

        if ($state->logType) {
            $handler->log($state);
        }

        return response(htmlspecialchars($state->logMessage), $statusCode ?: 200);
    }
}
