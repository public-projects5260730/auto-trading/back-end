<?php

namespace App\Http\Controllers\Pub;

use App\Http\Controllers\Controller;
use App\Models\UserLicense;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use ZipArchive;

class UserLicenseController extends Controller
{
    public function download(Request $request)
    {
        $code = $request->get('code');

        $query = query(UserLicense::class);
        $query->active();
        $query->where('license_key', $code);

        $userLicense = $query->firstOrFail();

        if (!Gate::allows('download', $userLicense)) {
            abort(403, __('permissions.do_not_have_permission'));
        }

        $botFiles = $userLicense->product->botFiles;
        if (empty($botFiles) || !is_array($botFiles)) {
            return redirect('/');
        }

        $storage = Storage::disk(config('voyager.storage.disk'));
        $fileName = "{$code}.zip";
        if (count($botFiles) == 1) {
            $botFile = reset($botFiles);
            if (!$storage->exists($botFile['download_link'])) {
                return redirect()->back();
            }
            $path = $storage->path($botFile['download_link']);
            return response()->download($path, $fileName);
        }

        $zip = new ZipArchive;
        $storage->makeDirectory('download/user-licenses');
        $zipPath = $storage->path("download/user-licenses/{$fileName}");

        if ($zip->open($zipPath, ZipArchive::CREATE) === TRUE) {
            foreach ($botFiles as $botFile) {
                if (!$storage->exists($botFile['download_link'])) {
                    continue;
                }
                $path = $storage->path($botFile['download_link']);
                $zip->addFile($path, $botFile['original_name']);
            }
            $zip->close();
        }

        return response()->download($zipPath, $fileName);
    }
}
