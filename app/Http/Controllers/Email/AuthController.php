<?php

namespace App\Http\Controllers\Email;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function verify(Request $request, string $id, string $hash)
    {
        $user = User::query()->findOrFail($id);

        if (!hash_equals(
            sha1($user->getEmailForVerification()),
            (string) $hash
        )) {
            throw new AuthorizationException();
        }

        if (!$user->hasVerifiedEmail()) {
            $user->markEmailAsVerified();
            event(new Verified($user));
        }
        return redirect('/');
    }
}
