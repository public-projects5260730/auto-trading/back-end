<?php

namespace App\Http\Controllers\Api;

use App\Models\PaymentProfile;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function image(Request $request)
    {
        $query = query(PaymentProfile::class);
        $query->whereNotNull('image')->orWhere('image', '!=', '');
        $query->casts();
        $query->active();
        $query->ordered();

        if ($request->exists('limit')) {
            $query->limit($request->get('limit'));
        }

        $payments = $query->get();
        return $this->response(compact('payments'));
    }
}
