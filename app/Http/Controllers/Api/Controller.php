<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\TraitCommon\Response;
use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController
{
    use Response;
}
