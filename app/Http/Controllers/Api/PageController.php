<?php

namespace App\Http\Controllers\Api;

use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index(Request $request)
    {
        $query = query(Page::class);
        $query->with('translations');
        $query->active()->casts();
        $query->filters();

        $pages = $query->get();
        return $this->response(compact('pages'));
    }

    public function view(Request $request, $slug)
    {
        $query = query(Page::class);
        $query->with('translations');
        $query->active()->casts();
        $query->filters(['slug' => $slug]);

        $page = $query->firstOrFail();

        return $this->response(compact('page'));
    }
}
