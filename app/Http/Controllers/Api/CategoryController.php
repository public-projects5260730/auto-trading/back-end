<?php

namespace App\Http\Controllers\Api;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        $query = Category::query();
        $query->casts();
        $query->ordered();
        $categories = $query->get();
        return $this->response(compact('categories'));
    }
}
