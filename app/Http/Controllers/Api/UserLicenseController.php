<?php

namespace App\Http\Controllers\Api;

use App\Models\UserLicense;
use App\Utils\Constants;
use Illuminate\Http\Request;

class UserLicenseController extends Controller
{
    public function index(Request $request)
    {
        $query = query(UserLicense::class);
        $query->with('product')->filters();
        $query->owner();
        $query->casts([
            'activated_at' => 'timestamp',
            'expired_at' => 'timestamp',
            'created_at' => 'timestamp',
            'updated_at' => 'timestamp',
        ]);
        $query->ordered();



        if ($request->exists('limit')) {
            $query->limit($request->get('limit'));
            $userLicenses = $query->get();
            return $this->response(compact('userLicenses'));
        }

        $paginate = $query->paginate($request->get('perPage', Constants::PER_PAGE));
        $paginate = paginator($paginate);

        return $this->response(compact('paginate'));
    }

    public function view(Request $request, int $id)
    {
        $query = query(UserLicense::class)->with('product', 'license');
        $query->owner();
        $query->casts();

        $userLicense = $query->firstOrFail();
        return $this->response(compact('userLicense'));
    }

    public function extend(Request $request, int $id)
    {
        return $this->error();
        $query = query(UserLicense::class)->with('product', 'license');
        $query->owner();

        $userLicense = $query->firstOrFail();
        // @todo check permission
        // @todo check params

        return $this->response(compact('userLicense'));
    }

    public function upgrade(Request $request, int $id)
    {
        return $this->error();
        $query = query(UserLicense::class)->with('product', 'license');
        $query->owner();

        $userLicense = $query->firstOrFail();

        // @todo check permission
        // @todo check params

        return $this->response(compact('userLicense'));
    }
}
