<?php

namespace App\Http\Controllers\Api;

use App\Models\ContactMessage;
use App\Models\User;
use App\Utils\Constants;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller
{
    public function sendMessage(Request $request)
    {
        $rules = [
            'title' => 'required|string|max:250',
            'message' => 'required|string|max:65000'
        ];
        if (!auth()->check()) {
            $rules += [
                'name' => 'required|string|max:250',
                'email' => 'required|email|max:250',
            ];
        }

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return $this->errors($validator->errors()->messages());
        }

        $validated = $validator->validated();
        $inputData = filter($validated, [
            'name' => 'str',
            'email' => 'str',
            'title' => 'str',
            'message' => 'str',
        ], true);

        if (auth()->check()) {
            /** @var User $user */
            $user = auth()->user();

            $inputData['user_id'] = $user->getKey();
            $inputData['name'] = $user->name;
            $inputData['email'] = $user->email;
        }

        $messageCount = query(ContactMessage::class)
            ->where('ip_address', $request->ip())
            ->whereDate('created_at', Carbon::today())
            ->count();

        if ($messageCount >= Constants::LIMIT_MESSAGES_PER_DAY) {
            return $this->error('You have reached your message limit for today.');
        }

        $inputData += [
            'ip_address' => $request->ip()
        ];

        $message = new ContactMessage();
        $message->mergeFillable(array_keys($inputData))
            ->fill($inputData)
            ->save();

        return $this->successfully();
    }
}
