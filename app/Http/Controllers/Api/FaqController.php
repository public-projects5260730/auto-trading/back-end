<?php

namespace App\Http\Controllers\Api;

use App\Models\FaqGroup;

class FaqController extends Controller
{
    public function groups()
    {
        $query = FaqGroup::query()->with('translations', 'faqs.translations')->filters();
        $query->ordered();
        $faqGroups = $query->get();

        return $this->response(compact('faqGroups'));
    }
}
