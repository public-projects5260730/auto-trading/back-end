<?php

namespace App\Http\Controllers\Api;

use App\Models\Faq;
use App\Models\License;
use App\Models\LiveTransaction;
use App\Models\PaymentProfile;
use App\Models\Product;
use App\Models\TrialLicenseRequest;
use App\Services\UserLicense;
use App\Utils\Constants;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        $query = query(Product::class);
        $query->select('products.*');
        $query->with('translations', 'accountLive.stats')->filters();
        $query->casts();
        $query->published();
        $query->orders();
        $query->ordered();

        if ($request->exists('limit')) {
            $query->limit($request->get('limit'));
            $products = $query->get();
            return $this->response(compact('products'));
        }

        $paginate = $query->paginate($request->get('perPage', Constants::PER_PAGE));
        $paginate = paginator($paginate);

        return $this->response(compact('paginate'));
    }

    public function view(Request $request, $slug)
    {
        $product = Product::query()
            ->with('translations', 'accountLive.stats')
            ->filters(['slug' => $slug])
            ->casts()
            ->published()
            ->firstOrFail();

        return $this->response(compact('product'));
    }

    public function faqs(Request $request, $slug)
    {
        $query = Faq::query()->with('translations')->filters(['products' => ['slug' => $slug]]);
        $query->ordered();
        $faqs = $query->get();

        return $this->response(compact('faqs'));
    }

    public function licenses(Request $request, $slug)
    {
        $query = License::query()->with('translations')->filters(['products' => ['slug' => $slug]]);
        $query->active()->ordered();
        $licenses = $query->get();

        return $this->response(compact('licenses'));
    }

    public function paymentGateways(Request $request, $slug)
    {
        $query = PaymentProfile::query()->filters(['products' => ['slug' => $slug]]);
        $query->active()->ordered();
        $paymentGateways = $query->get();

        return $this->response(compact('paymentGateways'));
    }

    public function transactions(Request $request, $slug)
    {
        $query = LiveTransaction::query()
            ->published()
            ->filters(['accountLive' => [
                'product' => ['slug' => $slug]
            ]])
            ->filters()
            ->orders();

        $transactions = $query->get();

        if ($request->exists('limit')) {
            $query->limit($request->get('limit'));
            $transactions = $query->get();
            return $this->response(compact('transactions'));
        }

        $paginate = $query->paginate($request->get('perPage', Constants::PER_PAGE));
        $paginate = paginator($paginate);

        return $this->response(compact('paginate'));
    }

    public function purchase(Request $request, $slug)
    {
        $rules = [
            'license_id' => ['required', 'numeric'],
            'payment_profile_id' => ['required', 'numeric'],
        ];

        if (!auth()->check()) {
            $rules += [
                'email' => 'required|email|max:250',
            ];
        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return $this->errors($validator->errors()->messages());
        }

        $product = Product::query()->filters(['slug' => $slug])->published()->firstOrFail();

        $parameters =  $validator->validated() + [
            'type' => 'license',
            'product_id' => $product->getKey(),
        ];

        if (auth()->check()) {
            $user = auth()->user();
            $parameters['email'] = $user->email;
        }

        return redirect()->route('api.purchases.pay', $parameters);
    }

    public function trialLicense(Request $request, $slug)
    {
        $product = Product::query()->filters(['slug' => $slug])->published()->firstOrFail();

        $requestCounts = query(TrialLicenseRequest::class)
            ->where('product_id', $product->getKey())
            ->where('ip_address', $request->ip())
            ->whereDate('created_at', Carbon::today())
            ->count();

        if ($requestCounts >= Constants::LIMIT_TRIAL_LICENSE_PER_DAY) {
            return $this->error('You have reached your trial license of this product limit for today.');
        }

        /** @var UserLicense $userLicenseService */
        $userLicenseService = resolve(UserLicense::class);

        $userLicense = $userLicenseService->createTrialLicense(auth()->user(), $product->getKey());
        if (empty($userLicense) || !$userLicense->license_key) {
            return $this->error();
        }

        $requestData = [
            'product_id' => $product->getKey(),
            'ip_address' => $request->ip(),
        ];

        $trialLicenseRequest = new TrialLicenseRequest();
        $trialLicenseRequest->mergeFillable(array_keys($requestData))
            ->fill($requestData)
            ->saveQuietly();

        return $this->response(compact('userLicense'));
    }
}
