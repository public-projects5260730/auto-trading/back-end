<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function me()
    {
        /** @var User $visitor */
        $visitor = auth()->user();
        // $visitor->append('permissions');
        return $this->response(compact('visitor'));
    }

    public function changeName(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
        ]);

        if ($validator->fails()) {
            return $this->errors($validator->errors()->messages());
        }

        $validated = $validator->validated();

        /** @var User $user */
        $user = auth()->user();
        $user->name = $validated['name'];
        $user->save();

        return $this->successfully(compact('user'));
    }

    public function changeEmail(Request $request)
    {
        /** @var User $user */
        $user = auth()->user();
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email', 'max:255', "unique:users,email,{$user->getKey()}"],
            'password' => ['required', 'string', 'min:8'],
        ]);

        if ($validator->fails()) {
            return $this->errors($validator->errors()->messages());
        }

        $validated = $validator->validated();

        if (!Hash::check($validated['password'], $user->password)) {
            return $this->errors(['password' => __('validation.current_password')]);
        }

        $user->email = $validated['email'];

        if ($user->isDirty('email')) {
            $user->email_verified_at = null;
        }

        $user->save();

        if ($user->isDirty('email') && $user instanceof MustVerifyEmail && !$user->hasVerifiedEmail()) {
            $user->sendEmailVerificationNotification();
        }

        return $this->successfully(compact('user'));
    }

    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => ['required', 'string', 'min:8'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        if ($validator->fails()) {
            return $this->errors($validator->errors()->messages());
        }

        $validated = $validator->validated();

        /** @var User $user */
        $user = auth()->user();

        if (!Hash::check($validated['old_password'], $user->password)) {
            return $this->errors(['old_password' => __('validation.current_password')]);
        }

        $user->password = bcrypt($validated['password']);
        $user->save();

        return $this->successfully(compact('user'));
    }
}
