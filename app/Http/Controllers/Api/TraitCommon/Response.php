<?php

namespace App\Http\Controllers\Api\TraitCommon;

use Illuminate\Http\Response as HttpResponse;

trait Response
{
    protected function successfully($data = null)
    {
        $data = is_array($data) ? $data : func_get_args();
        return $this->response([
            'successfully' => true,
        ] + $data);
    }

    protected function error($error = null, $status = HttpResponse::HTTP_FORBIDDEN)
    {
        return $this->response(['error' => $error ?? __('permissions.do_not_have_permission')], $status);
    }

    protected function errors($errors = null, $status = HttpResponse::HTTP_BAD_REQUEST)
    {
        $errors = is_array($errors) ? $errors : func_get_args();
        return $this->response(['errors' => $errors], $status);
    }

    protected function response($data = [], $status = HttpResponse::HTTP_OK)
    {
        return response()->json($data, $status);
    }
}
