<?php

namespace App\Http\Controllers\Api;

use App\Models\Broker;
use App\Utils\Constants;
use Illuminate\Http\Request;

class BrokerController extends Controller
{
    public function index(Request $request)
    {
        $query = Broker::query()->filters();
        $query->casts();
        $query->published();
        $query->ordered();

        if ($request->exists('limit')) {
            $query->limit($request->get('limit'));
            $brokers = $query->get();
            return $this->response(compact('brokers'));
        }

        $paginate = $query->paginate($request->get('perPage', Constants::PER_PAGE));
        $paginate = paginator($paginate);
        return $this->response(compact('paginate'));
    }
}
