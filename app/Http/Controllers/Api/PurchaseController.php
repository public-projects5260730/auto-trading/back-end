<?php

namespace App\Http\Controllers\Api;

use App\Models\PurchaseRequest;
use App\Payments\AbstractProvider;
use App\Services\PurchaseRequest as ServicesPurchaseRequest;

class PurchaseController extends Controller
{
    public function pay($type)
    {
        $purchasable = purchasable($type);
        $purchase = $purchasable->getPurchaseFromRequest(auth()->user(), $error);
        if (!$purchase) {
            return $this->error($error);
        }

        /** @var ServicesPurchaseRequest $purchaseRequestService */
        $purchaseRequestService = resolve(ServicesPurchaseRequest::class);
        $purchaseRequest = $purchaseRequestService->insertPurchaseRequest($purchase);

        /** @var  AbstractProvider $providerHandler*/
        $providerHandler = $purchase->paymentProfile->providerHandler;
        return $providerHandler->initiatePayment($purchaseRequest, $purchase);
    }

    public function process()
    {
        $filters = request()->only(['request_key']);
        $purchaseRequest = PurchaseRequest::query()->filters($filters)->firstOrFail();

        $paymentProfile = $purchaseRequest->paymentProfile;
        if (!$paymentProfile) {
            return $this->error(__('purchase_request_contains_invalid_payment_profile'));
        }

        $purchasableHandler = purchasable($purchaseRequest->purchasable_type_id);
        $purchase = $purchasableHandler->getPurchaseFromExtraData($purchaseRequest->extra_data, $paymentProfile, auth()->user(), $error);
        if (!$purchase) {
            return $this->error($error);
        }

        /** @var  AbstractProvider $providerHandler*/
        $providerHandler = $purchase->paymentProfile->providerHandler;

        $result = $providerHandler->processPayment($purchaseRequest, $paymentProfile, $purchase);
        if (!$result) {
            return $this->response([
                'redirect' => $purchase->returnUrl
            ]);
        }

        return $this->response($result);
    }
}
