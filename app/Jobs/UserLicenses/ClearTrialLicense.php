<?php

namespace App\Jobs\UserLicenses;

use App\Models\UserLicense;
use App\Services\Api\BotLicense;
use App\Utils\Constants;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;

class ClearTrialLicense implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var UserLicense
     */
    protected $userLicense;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(UserLicense $userLicense)
    {
        $this->userLicense = $userLicense;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $userLicense = $this->userLicense;

        if ($userLicense->expired_at && $userLicense->expired_at >= Carbon::today()) {
            return;
        }

        if ($userLicense->activated_at) {
            $expiredDate = Carbon::today()->addDays(- (max(1, $userLicense->license_days)));
            if ($userLicense->activated_at >= $expiredDate) {
                return;
            }
        }

        $expiredDate = Carbon::today()->addDays(- (max(1, config('licenses.trial.license_days',  1))));
        if ($userLicense->created_at >= $expiredDate) {
            return;
        }

        if (in_array($userLicense->state, Arr::only(Constants::USER_LICENSE_STATES, ['NEW', 'ACTIVATED']))) {
            $successfully = $this->cancelBotLicense();
            if (empty($successfully)) {
                return;
            }
        }

        $userLicense->delete();
    }

    protected function cancelBotLicense()
    {
        $userLicense = $this->userLicense;
        $userLicense->reason = 'AutoTrading send DELETE TRIAL USER LICENSE';
        $botLicenseReponse = $this->getBotLicenseApi()->cancel($userLicense);
        return !empty($botLicenseReponse['IsSuccess']);
    }

    protected function getBotResponse($oldBotReponse, array $addNew)
    {
        if (empty($oldBotReponse) || !is_array($oldBotReponse)) {
            $oldBotReponse = [];
        }

        return $oldBotReponse + $addNew;
    }

    /**
     *
     * @return BotLicense
     */
    protected function getBotLicenseApi()
    {
        return resolve(BotLicense::class);
    }
}
