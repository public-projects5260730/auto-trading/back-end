<?php

namespace App\Jobs\UserLicenses;

use App\Models\AsyncBotLicense;
use App\Models\UserLicense;
use App\Services\Api\BotLicense;
use App\Utils\Constants;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;

class AsyncReportBotLicense implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Collection
     */
    protected $userLicenses;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Collection $userLicenses)
    {
        $this->userLicenses = $userLicenses;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $userLicenses = $this->userLicenses;
        if (!$userLicenses || !$userLicenses->count()) {
            return;
        }

        $keys = $userLicenses->keyBy('license_key')->keys()->toArray();

        $botRes = $this->getBotLicenseApi()->list($keys);
        if (empty($botRes['Data']) || !is_array($botRes['Data'])) {
            return;
        }

        $botLicenses = Arr::get($botRes, 'Data', []);

        foreach ($botLicenses as $botLicense) {
            if (empty($botLicense['LicenseCode'])) {
                continue;
            }

            $LicenseCode = Arr::get($botLicense, 'LicenseCode');
            $userLicense = $userLicenses->first(function ($userLicense) use ($LicenseCode) {
                return $userLicense->license_key == $LicenseCode;
            });

            if (!$userLicense) {
                continue;
            }

            $ActiveAt = Arr::get($botLicense, 'ActiveAt', 0);
            $ExpiredAt = Arr::get($botLicense, 'ExpiredAt', 0);

            /** @var UserLicense $userLicense */
            $userLicense->state = $this->mapBotStateToUserState(Arr::get($botLicense, 'Status', 0));
            $userLicense->account_number = Arr::get($botLicense, 'AmountAccount', 0);
            $userLicense->license_days = Arr::get($botLicense, 'AmountDay', 0);
            $userLicense->activated_accounts = Arr::get($botLicense, 'ActiveAccountIds', []);
            $userLicense->activated_at = empty($ActiveAt) ? null : Carbon::parse($ActiveAt)->toDateTimeString();
            $userLicense->expired_at = empty($ExpiredAt) ? null : Carbon::parse($ExpiredAt)->toDateTimeString();
            $userLicense->is_trial = Arr::get($botLicense, 'IsTrial') ? 1 : 0;

            if ($userLicense->isDirty('state') && $userLicense->state == Constants::USER_LICENSE_STATES['EXPIRED']) {
                $userLicense->reason = Arr::get($botLicense, 'CancelReasonContent', '');
            }

            if (
                $userLicense->isDirty(['account_number', 'license_days', 'is_trial'])
                || ($userLicense->isDirty('state')
                    && (!in_array($userLicense->state, Arr::only(Constants::USER_LICENSE_STATES, ['ACTIVATED', 'EXPIRED']))
                    ))
            ) {

                $changed = $userLicense->getDirty();

                $asyncBotLicenseModel = new AsyncBotLicense();
                $asyncBotLicenseModel->license_key = $LicenseCode;
                $asyncBotLicenseModel->data = [
                    'changed' => $changed,
                    'original' => Arr::only($userLicense->getOriginal(), array_keys($changed)),
                    'BotReponse' => $botLicense
                ];

                $asyncBotLicenseModel->saveQuietly();
                continue;
            }

            if ($userLicense->isDirty()) {
                $userLicense->bot_response = $this->getBotResponse($userLicense->bot_response, ['sync_bot_license' . time() => $botLicense]);
                $userLicense->save();
            }
        }
    }

    protected function getBotResponse($oldBotReponse, array $addNew)
    {
        if (empty($oldBotReponse) || !is_array($oldBotReponse)) {
            $oldBotReponse = [];
        }

        return $oldBotReponse + $addNew;
    }

    protected function mapBotStateToUserState(int $botState)
    {
        foreach (Constants::BOT_LICENSE_STATES as $key => $state) {
            if ($state == $botState) {
                return Arr::get(Constants::USER_LICENSE_STATES, $key, 99);
            }
        }
        return 99;
    }

    /**
     *
     * @return BotLicense
     */
    protected function getBotLicenseApi()
    {
        return resolve(BotLicense::class);
    }
}
