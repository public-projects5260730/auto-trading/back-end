<?php

namespace App\Jobs\UserLicenses;

use App\Models\UserLicense;
use App\Services\Api\BotLicense;
use App\Utils\Constants;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Arr;

class SyncBotLicense implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var UserLicense
     */
    protected $userLicense;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(UserLicense $userLicense)
    {
        $this->userLicense = $userLicense;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $userLicense = $this->userLicense;

        if (in_array($userLicense->state, Arr::only(Constants::USER_LICENSE_STATES, ['NEW', 'ACTIVATED']))) {
            if (!$userLicense->license_key) {
                $this->createBotLicense();
            } else {
                $this->updateBotLicense();
            }
        } elseif ($userLicense->license_key) {
            $this->cancelBotLicense();
        }
    }

    protected function createBotLicense()
    {
        $userLicense = $this->userLicense;
        $botLicenseReponse = $this->getBotLicenseApi()->create($userLicense);

        $userLicense->bot_response = $this->getBotResponse($userLicense->bot_response, [
            'sync_create_' . time() => $botLicenseReponse
        ]);

        if (!empty($botLicenseReponse['LicenseCode'])) {
            $userLicense->license_key = $botLicenseReponse['LicenseCode'];
            $userLicense->is_synced = true;
            $userLicense->saveQuietly();
        }
    }

    protected function updateBotLicense()
    {
        $userLicense = $this->userLicense;
        $botLicenseReponse = $this->getBotLicenseApi()->extend($userLicense);
        $userLicense->bot_response = $this->getBotResponse($userLicense->bot_response, [
            'sync_extend_' . time() => $botLicenseReponse
        ]);

        if (!empty($botLicenseReponse['IsSuccess'])) {
            $userLicense->is_synced = true;
            $userLicense->saveQuietly();
        }
    }

    protected function cancelBotLicense()
    {
        $userLicense = $this->userLicense;
        $botLicenseReponse = $this->getBotLicenseApi()->cancel($userLicense);
        $userLicense->bot_response = $this->getBotResponse($userLicense->bot_response, [
            'sync_cancel_' . time() => $botLicenseReponse
        ]);

        if (!empty($botLicenseReponse['IsSuccess'])) {
            $userLicense->is_synced = true;
            $userLicense->saveQuietly();
        }
    }

    protected function getBotResponse($oldBotReponse, array $addNew)
    {
        if (empty($oldBotReponse) || !is_array($oldBotReponse)) {
            $oldBotReponse = [];
        }

        return $oldBotReponse + $addNew;
    }

    /**
     *
     * @return BotLicense
     */
    protected function getBotLicenseApi()
    {
        return resolve(BotLicense::class);
    }
}
