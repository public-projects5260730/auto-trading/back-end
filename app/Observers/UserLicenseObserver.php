<?php

namespace App\Observers;

use App\Events\Notifications\UserLicense\Canceled;
use App\Events\Notifications\UserLicense\Downgrade;
use App\Events\Notifications\UserLicense\Expired;
use App\Events\Notifications\UserLicense\Purchased;
use App\Events\Notifications\UserLicense\Revoked;
use App\Events\Notifications\UserLicense\Upgrade;
use App\Models\UserLicense;
use App\Services\Api\BotLicense;
use App\Utils\Constants;

class UserLicenseObserver
{
    public function creating(UserLicense $userLicense)
    {
        $botLicenseReponse = $this->getBotLicenseApi()->create($userLicense);
        $userLicense->bot_response = [
            'creating_create_' . time() => $botLicenseReponse
        ];
        if (!empty($botLicenseReponse['LicenseCode'])) {
            $userLicense->license_key = $botLicenseReponse['LicenseCode'];
            $userLicense->is_synced = true;
        }
    }

    /**
     * Handle the UserLicense "created" event.
     *
     * @param  \App\Models\UserLicense  $userLicense
     * @return void
     */
    public function created(UserLicense $userLicense)
    {
        if ($userLicense->license_key) {
            event(new Purchased($userLicense));
        }
    }

    public function updating(UserLicense $userLicense)
    {
        if ($userLicense->license_key) {
            if ($userLicense->isDirty('account_number', 'license_days', 'is_trial')) {
                $userLicense->is_synced = false;

                $botLicenseReponse = $this->getBotLicenseApi()->extend($userLicense);
                $botResponse = $userLicense->bot_response;
                if (empty($botResponse) || !is_array($botResponse)) {
                    $botResponse = [];
                }
                $botResponse += [
                    'updating_extend' . time() => $botLicenseReponse
                ];

                $userLicense->bot_response = $botResponse;
                if (!empty($botLicenseReponse['IsSuccess'])) {
                    $userLicense->is_synced = true;
                }
            } elseif ($userLicense->isDirty('state')) {
                switch ($userLicense->state) {
                    case Constants::USER_LICENSE_STATES['EXPIRED']:
                    case Constants::USER_LICENSE_STATES['CANCELED']:
                    case Constants::USER_LICENSE_STATES['REVOKED']:

                        $userLicense->is_synced = false;

                        $botLicenseReponse = $this->getBotLicenseApi()->cancel($userLicense);
                        $botResponse = $userLicense->bot_response;
                        if (empty($botResponse) || !is_array($botResponse)) {
                            $botResponse = [];
                        }
                        $botResponse += [
                            'updating_cancel_' . time() => $botLicenseReponse
                        ];

                        $userLicense->bot_response = $botResponse;
                        if (!empty($botLicenseReponse['IsSuccess'])) {
                            $userLicense->is_synced = true;
                        }
                        break;
                }
            }
        }
    }
    /**
     * Handle the UserLicense "updated" event.
     *
     * @param  \App\Models\UserLicense  $userLicense
     * @return void
     */
    public function updated(UserLicense $userLicense)
    {
        if ($userLicense->isDirty('license_key')) {
            event(new Purchased($userLicense));
            return;
        }

        if ($userLicense->isDirty(['account_number'])) {
            $newVal = $userLicense->account_number;
            $oldVal = $userLicense->getOriginal('account_number');

            if ($newVal < 0 || $newVal > $oldVal) {
                event(new Upgrade($userLicense));
            } else if ($oldVal < 0 || $newVal < $oldVal) {
                event(new Downgrade($userLicense));
            }
            return;
        }

        if ($userLicense->isDirty(['license_days'])) {
            $newVal = $userLicense->license_days;
            $oldVal = $userLicense->getOriginal('license_days');

            if ($newVal < 0 || $newVal > $oldVal) {
                event(new Upgrade($userLicense));
            } else if ($oldVal < 0 || $newVal < $oldVal) {
                event(new Downgrade($userLicense));
            }
            return;
        }

        if ($userLicense->isDirty('state')) {
            switch ($userLicense->state) {
                case Constants::USER_LICENSE_STATES['EXPIRED']:
                    event(new Expired($userLicense));
                    break;

                case Constants::USER_LICENSE_STATES['CANCELED']:
                    event(new Canceled($userLicense));
                    break;

                case Constants::USER_LICENSE_STATES['REVOKED']:
                    event(new Revoked($userLicense));
                    break;
            }
            return;
        }
    }

    /**
     *
     * @return BotLicense
     */
    protected function getBotLicenseApi()
    {
        return resolve(BotLicense::class);
    }
}
