<?php

namespace App\Exceptions;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $exception)
    {
        if ($request->segment(1) == 'api') {
            if ($exception instanceof NotFoundHttpException || $exception instanceof MethodNotAllowedHttpException) {
                $statusCode = $exception->getStatusCode() ?: Response::HTTP_NOT_FOUND;
                $message = $exception->getMessage() ?: __('requested.not_found');
            } elseif ($exception instanceof ModelNotFoundException) {
                $statusCode = $exception->getCode() ?: Response::HTTP_NOT_FOUND;
                $modelName = Str::snake(Str::pluralStudly(class_basename($exception->getModel())), ' ');
                $message = __('requested.model_not_found', ['model' => $modelName]);
            } else {
                $statusCode = $exception->getCode();
                if (config('app.debug')) {
                    $message = $exception->getMessage();
                } elseif (!empty($statusCode) && in_array($statusCode, [401, 403, 423, 511])) {
                    $message = $exception->getMessage();
                } else {
                    $message = __('requested.oops_we_ran_into_some_problems');
                }
            }
            return response()->json([
                'error' => $message,
            ], $statusCode ?: Response::HTTP_INTERNAL_SERVER_ERROR);
        }
        if (!in_array($request->segment(1), ['admin']) &&  $exception instanceof NotFoundHttpException) {
            View::addExtension('html', 'php');
            return response(view('public::index'));
        }

        return parent::render($request, $exception);
    }

    protected function shouldntReport(Throwable $e)
    {
        $statusCode = $e->getCode();
        if (!empty($statusCode) && in_array($statusCode, [401, 423, 511])) {
            return true;
        }

        return parent::shouldntReport($e);
    }
}
