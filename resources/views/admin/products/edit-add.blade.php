@php
    $edit = !is_null($dataTypeContent->getKey());
    $add = is_null($dataTypeContent->getKey());
@endphp

@extends('voyager::bread.edit-add')

@section('content')
    <div>
        <div class="page-content container-fluid">
            <!-- form start -->
            <form role="form" class="form-edit-add"
                action="{{ $edit ? route('voyager.' . $dataType->slug . '.update', $dataTypeContent->getKey()) : route('voyager.' . $dataType->slug . '.store') }}"
                method="POST" enctype="multipart/form-data">
                <!-- PUT Method if we are editing -->
                @if ($edit)
                    {{ method_field('PUT') }}
                @endif

                <!-- CSRF TOKEN -->
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-md-8">
                        <!-- ### ERRORS ### -->

                        @if (count($errors) > 0)
                            <div class="panel">
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        @endif

                        <!-- ### TITLE ### -->
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <i class="voyager-character"></i> {!! __('products.title') !!}
                                </h3>
                                <div class="panel-actions">
                                    <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                        aria-hidden="true"></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <x-admin.voyager.fields :dataType="$dataType" :dataTypeContent="$dataTypeContent" fields="title"
                                    :hideLabel="true" />
                            </div>
                        </div>

                        <!-- ### EXCERPT ### -->
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">{!! __('voyager::post.excerpt') !!}</h3>
                                <div class="panel-actions">
                                    <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                        aria-hidden="true"></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <x-admin.voyager.fields :dataType="$dataType" :dataTypeContent="$dataTypeContent" fields="excerpt"
                                    :hideLabel="true" />
                            </div>
                        </div>

                        <!-- ### TABS ### -->
                        <div class="panel">
                            <div class="panel-heading">
                                <h3 class="panel-title">{!! __('products.infomation') !!}</h3>
                                <div class="panel-actions">
                                    <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                        aria-hidden="true"></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <ul class="nav nav-tabs">
                                    <li class="nav-item active">
                                        <a data-toggle="tab" href="#overviewTab">{{ __('products.tabs.overview') }}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a data-toggle="tab" href="#backtestTab">{{ __('products.tabs.backtest') }}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a data-toggle="tab" href="#commentsTab">{{ __('products.tabs.comments') }}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a data-toggle="tab" href="#versionsTab">{{ __('products.tabs.what_the_news') }}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a data-toggle="tab" href="#guideTab">{{ __('products.tabs.manuel_guide') }}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a data-toggle="tab" href="#faqsTab">{{ __('products.tabs.faqs') }}</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div id="overviewTab" class="tab-pane fade in active">
                                        <div class="row">
                                            <x-admin.voyager.fields :dataType="$dataType" :dataTypeContent="$dataTypeContent" fields="overview"
                                                :hideLabel="true" />
                                        </div>
                                    </div>
                                    <div id="backtestTab" class="tab-pane fade in">
                                        <div class="row">
                                            <x-admin.voyager.fields :dataType="$dataType" :dataTypeContent="$dataTypeContent"
                                                fields="backtest_images" :hideLabel="true" />
                                        </div>
                                    </div>

                                    <div id="commentsTab" class="tab-pane fade in">
                                        <div class="row">
                                            <x-admin.voyager.fields :dataType="$dataType" :dataTypeContent="$dataTypeContent" fields="comments"
                                                :hideLabel="true" />
                                        </div>
                                    </div>

                                    <div id="versionsTab" class="tab-pane fade in">
                                        <div class="row">
                                            <x-admin.voyager.fields :dataType="$dataType" :dataTypeContent="$dataTypeContent" fields="versions"
                                                :hideLabel="true" />
                                        </div>
                                    </div>

                                    <div id="guideTab" class="tab-pane fade in">
                                        <div class="row">
                                            <x-admin.voyager.fields :dataType="$dataType" :dataTypeContent="$dataTypeContent" fields="guide"
                                                :hideLabel="true" />
                                        </div>
                                    </div>
                                    <div id="faqsTab" class="tab-pane fade in">
                                        <div class="row">
                                            <x-admin.voyager.fields :dataType="$dataType" :dataTypeContent="$dataTypeContent" :fields="['faqs', 'product_belongstomany_faq_relationship']"
                                                :hideLabel="true" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-4">
                        <!-- ### DETAILS ### -->
                        <div class="panel panel panel-bordered panel-warning">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="icon wb-clipboard"></i> {{ __('products.details') }}</h3>
                                <div class="panel-actions">
                                    <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                        aria-hidden="true"></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <x-admin.voyager.fields :dataType="$dataType" :dataTypeContent="$dataTypeContent" :fields="[
                                        'order',
                                        'slug',
                                        'status',
                                        'category_id',
                                        'product_belongsto_category_relationship',
                                        'featured',
                                    ]" />
                                </div>
                            </div>
                        </div>

                        <!-- ### IMAGE ### -->
                        <div class="panel panel-bordered panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="icon wb-image"></i> {{ __('products.image') }}</h3>
                                <div class="panel-actions">
                                    <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                        aria-hidden="true"></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <x-admin.voyager.fields :dataType="$dataType" :dataTypeContent="$dataTypeContent" fields="image"
                                    :hideLabel="true" />
                            </div>
                        </div>

                        <!-- ### BOT INFO ### -->
                        <div class="panel panel-bordered panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="icon wb-image"></i> {{ __('products.bot_detail') }}
                                </h3>
                                <div class="panel-actions">
                                    <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                        aria-hidden="true"></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <x-admin.voyager.fields :dataType="$dataType" :dataTypeContent="$dataTypeContent" :fields="['files', 'product_belongsto_account_live_relationship']" />
                            </div>
                        </div>

                        <!-- ### PAYMENTS ### -->
                        <div class="panel panel-bordered panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="icon wb-image"></i> {{ __('products.payments') }}
                                </h3>
                                <div class="panel-actions">
                                    <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                        aria-hidden="true"></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <x-admin.voyager.fields :dataType="$dataType" :dataTypeContent="$dataTypeContent" :fields="[
                                    'product_belongstomany_license_relationship',
                                    'product_belongstomany_payment_profile_relationship',
                                ]" />
                            </div>
                        </div>

                        <!-- ### OTHER FIELDS ### -->
                        <div class="panel panel-bordered panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">{{ __('voyager::post.additional_fields') }}</h3>
                                <div class="panel-actions">
                                    <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                        aria-hidden="true"></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <x-admin.voyager.fields :dataType="$dataType" :dataTypeContent="$dataTypeContent" :excludeFields="[
                                    'title',
                                    'excerpt',
                                    'overview',
                                    'backtest_images',
                                    'comments',
                                    'versions',
                                    'guide',
                                    'faqs',
                                    'product_belongstomany_faq_relationship',
                                    'slug',
                                    'status',
                                    'product_belongsto_category_relationship',
                                    'featured',
                                    'image',
                                    'order',
                                    'category_id',
                                    'product_belongsto_account_live_relationship',
                                    'files',
                                    'product_belongstomany_license_relationship',
                                    'product_belongstomany_payment_profile_relationship',
                                ]" />
                            </div>
                        </div>

                    </div>
                </div>

                <div class="panel-footer text-right">
                @section('submit-buttons')
                    <button type="submit" class="btn btn-primary save">{{ __('voyager::generic.save') }}</button>
                @stop
                @yield('submit-buttons')
            </div>
        </form>

        <div style="display:none">
            <input type="hidden" id="upload_url" value="{{ route('voyager.upload') }}">
            <input type="hidden" id="upload_type_slug" value="{{ $dataType->slug }}">
        </div>
    </div>

    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}
                    </h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'
                    </h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default"
                        data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger"
                        id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Delete File Modal -->
@stop
