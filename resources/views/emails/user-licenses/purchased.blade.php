<h2>Hello <b>{{ $user ? $user->name : '' }}</b></h2>
<p>
    @if ($userLicense->license)
        You have successfully purchased license <b>{{ $userLicense->license->title }}</b> of the
        <b>{{ $product->title }}</b> auto-trading product.
    @else
        You have received a trial license of the <b>{{ $product->title }}</b> auto-trading product.
    @endif
</p>

<p>
    <b>License information.</b>
<ul>
    <li>
        License Key: <b>{{ $userLicense->license_key }}</b>
    </li>
    <li>
        Number of using days: <b>
            @if ($userLicense->license_days < 0)
                Unlimited
            @else
                {{ $userLicense->license_days }} days
            @endif
        </b>
    </li>
    <li>
        Account Number: <b>
            @if ($userLicense->account_number < 0)
                Unlimited
            @else
                {{ $userLicense->account_number }}
            @endif
        </b>
    </li>
</ul>
</p>

<p>
    We sincerely thank you for your concern and cooperation!
</p>
<p>Regards, <br /> {{ config('app.name') }}</p>
