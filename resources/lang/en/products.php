<?php

return [
    'title' => 'Title',
    'infomation' => 'Infomation',
    'details' => 'Details',
    'image' => 'Image',
    'bot_detail' => 'Bot Details',
    'payments' => 'Payments',
    'tabs' => [
        'overview' => 'Overview',
        'backtest' => 'Backtest',
        'comments' => 'Comments',
        'what_the_news' => 'What The News',
        'manuel_guide' => 'Manuel Guide',
        'faqs' => 'FAQs',
    ]
];
