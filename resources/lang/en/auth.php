<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'token_invalid' => 'Token is Invalid.',
    'token_not_found' => 'Authorization Token not found.',
    'email_password_is_incorrect' => 'Email or password is incorrect.',
    'please_active_your_acction_before_do_this_action' => 'Before you can login, you must active your account with the link sent to your email address.',
];
