<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Requested Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the requested broker for a requested update attempt
    | has failed, such as for an invalid token or invalid new requested.
    |
    */

    'oops_we_ran_into_some_problems' => 'Oops! We ran into some problems.',
    'model_not_found' => 'The requested :model could not be found.',
    'not_found' => 'The requested could not be found.',
    'payment_provider_not_found' => 'Payment provider not found.',
    'purchasable_not_found' => 'purchasable not found.',
];
