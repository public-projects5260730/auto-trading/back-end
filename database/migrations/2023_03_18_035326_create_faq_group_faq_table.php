<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFaqGroupFaqTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faq_group_faq', function (Blueprint $table) {
            $table->integer('faq_group_id')->unsigned()->index();
            $table->integer('faq_id')->unsigned()->index();
            $table->primary(['faq_group_id', 'faq_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faq_group_faq');
    }
}
