<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentProviderLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_provider_logs', function (Blueprint $table) {
            $table->id();
            $table->string('purchase_request_key', 50)->nullable();
            $table->string('provider_id', 50);
            $table->string('transaction_id', 100)->index()->nullable();
            $table->string('subscriber_id', 100)->index()->nullable();
            $table->enum('log_type', ['payment', 'cancel', 'info', 'error'])->index();
            $table->string('log_message', 255)->default('');
            $table->json('log_details')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_provider_logs');
    }
}
