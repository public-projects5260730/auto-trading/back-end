<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('slug')->unique();
            $table->integer('category_id')->unsigned()->index()->default(0);
            $table->text('excerpt')->nullable();
            $table->mediumText('overview')->nullable();
            $table->mediumText('guide')->nullable();
            $table->mediumText('comments')->nullable();
            $table->mediumText('versions')->nullable();
            $table->string('image')->nullable();
            $table->bigInteger('account_live_id')->unsigned()->index()->default(0);
            $table->json('files')->nullable();
            $table->json('backtest_images')->nullable();
            $table->tinyInteger('status')->default(2);
            $table->boolean('featured')->default(0);
            $table->integer('order')->unsigned()->default(10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
