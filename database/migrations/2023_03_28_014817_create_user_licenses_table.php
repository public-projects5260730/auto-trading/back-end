<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserLicensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_licenses', function (Blueprint $table) {
            $table->id();
            $table->string('license_key', 50)->unique()->nullable();
            $table->bigInteger('user_id')->unsigned()->index();
            $table->bigInteger('product_id')->unsigned()->index();
            $table->bigInteger('license_id')->unsigned()->index();
            $table->string('purchase_request_key', 50)->nullable();
            $table->integer('account_number');
            $table->integer('license_days');
            $table->json('activated_accounts')->nullable();
            $table->tinyInteger('state')->unsigned()->default(1)->comment('1: new, 2: activated, 3: expired, 4: canceled, 5: revoked');
            $table->text('reason')->nullable();
            $table->json('extra')->nullable();
            $table->timestamp('activated_at')->nullable();
            $table->timestamp('expired_at')->nullable();
            $table->boolean('is_trial')->default(false);
            $table->boolean('is_synced')->default(false);
            $table->json('upgrade')->nullable();
            $table->json('bot_response')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_licenses');
    }
}
