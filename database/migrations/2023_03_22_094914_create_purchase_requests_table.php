<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_requests', function (Blueprint $table) {
            $table->id();
            $table->string('request_key', 50)->unique();
            $table->bigInteger('user_id')->unsigned()->default(0);
            $table->string('provider_id', 50);
            $table->bigInteger('payment_profile_id');
            $table->string('purchasable_type_id', 50);
            $table->decimal('cost_amount', 23, 8, true);
            $table->string('cost_currency', 5);
            $table->json('extra_data')->nullable();
            $table->string('provider_metadata', 100)->nullable();
            $table->index(['provider_id', 'provider_metadata'], 'provider_metadata');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_requests');
    }
}
