<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLicensesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('licenses', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->integer('account_number');
            $table->integer('license_days');
            $table->decimal('cost_amount', 23, 8, true);
            $table->string('cost_currency', 5);
            $table->boolean('is_trial')->default(false);
            $table->integer('order')->unsigned()->index()->default(10);
            $table->boolean('active')->index()->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('licenses');
    }
}
