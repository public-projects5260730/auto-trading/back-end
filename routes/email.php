<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Email Routes
|--------------------------------------------------------------------------
|
| Here is where you can register email to verification routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('auth')->group(function () {
    Route::get('verify/{id}/{hash}', 'AuthController@verify')->middleware(['signed'])->name('verification.verify');
});
