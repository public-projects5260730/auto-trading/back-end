<?php

use Illuminate\Support\Facades\Route;
use TCG\Voyager\Facades\Voyager;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('user-licenses')->name('userLicenses.')->group(function () {
    Route::prefix('{id}')->where(['id' => '[0-9]+'])->group(function () {
        Route::get('email-purchased', 'UserLicenseController@emailPurchased')->name('emailPurchased');
    });

    Route::get('category', 'PostController@category')->name('category');
});


Voyager::routes();
