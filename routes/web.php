<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::withoutMiddleware('web')->group(function () {
    Route::any('payments/{providerId}/callback', 'PaymentController@callback')
        ->where(['providerId' => '[a-zA-Z0-9-_]+'])
        ->name('payments.callback');
});

Route::prefix('user-licenses')->name('licenses.')->group(function () {
    Route::get('download', 'UserLicenseController@download')->name('download');
});

Route::get('{any}', function () {
    View::addExtension('html', 'php');
    return view('public::index');
})->where('any', '.*')->name('home');


Route::prefix('products')->name('products.')->group(function () {
    Route::prefix('{slug}')->group(function () {
        Route::get('view', null)->name('view');
    });
});

Route::prefix('purchases')->name('purchases.')->group(function () {
    Route::prefix('{type}')->group(function () {
        Route::get('purchased', null)->name('purchased');
    });
});

Route::prefix('purchases')->name('purchases.')->group(function () {
    Route::prefix('{type}')->group(function () {
        Route::get('purchased', null)->name('purchased');
    });
});

Route::get('/reset-password/{token}', null)->name('password.reset');
