<?php

use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->name('auth.')->group(function () {
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
    Route::post('refresh-token', 'AuthController@refresh');
    Route::post('forgot-password', 'AuthController@forgotPassword');
    Route::post('reset-password', 'AuthController@resetPassword');
});

Route::prefix('pages')->name('pages.')->group(function () {
    Route::get('', 'PageController@index')->name('index');
    Route::prefix('{slug}')->group(function () {
        Route::get('view', 'PageController@view')->name('view');
    });
});

Route::prefix('products')->name('products.')->group(function () {
    Route::get('', 'ProductController@index')->name('index');
    Route::get('sorted', 'ProductController@index')->name('sorted');

    Route::prefix('{slug}')->group(function () {
        Route::get('view', 'ProductController@view')->name('view');
        Route::get('faqs', 'ProductController@faqs')->name('faqs');
        Route::get('licenses', 'ProductController@licenses')->name('licenses');
        Route::get('payment-gateways', 'ProductController@paymentGateways')->name('paymentGateways');
        Route::get('transactions', 'ProductController@transactions')->name('transactions');
        Route::post('purchase', 'ProductController@purchase');
        Route::post('trial-license', 'ProductController@trialLicense');
    });
});

Route::prefix('purchases')->name('purchases.')->group(function () {
    Route::get('process', 'PurchaseController@process')->name('process');
    Route::prefix('{type}')->group(function () {
        Route::get('pay', 'PurchaseController@pay')->name('pay');
    });
});

Route::prefix('categories')->name('categories.')->group(function () {
    Route::get('', 'CategoryController@index')->name('index');
});

Route::prefix('brokers')->name('brokers.')->group(function () {
    Route::get('', 'BrokerController@index')->name('index');
});

Route::prefix('faqs')->name('faqs.')->group(function () {
    Route::get('groups', 'FaqController@groups')->name('groups');
});

Route::prefix('contacts')->name('contacts.')->group(function () {
    Route::post('send-message', 'ContactController@sendMessage');
});

Route::prefix('payments')->name('payments.')->group(function () {
    Route::get('image', 'PaymentController@image')->name('index');
});

Route::middleware(['auth.jwt:jwt'])->group(function () {
    Route::prefix('auth')->name('auth.')->group(function () {
        Route::post('logout', 'AuthController@logout');
    });

    Route::prefix('users')->name('users.')->group(function () {
        Route::get('me', 'UserController@me')->name('me');
        Route::post('change-name', 'UserController@changeName');
        Route::post('change-email', 'UserController@changeEmail');
        Route::post('change-password', 'UserController@changePassword');
    });

    Route::prefix('user-licenses')->name('userLicenses.')->group(function () {
        Route::get('', 'UserLicenseController@index')->name('index');
        Route::prefix('{id}')->where(['id' => '[0-9]+'])->group(function () {
            Route::get('view', 'UserLicenseController@view')->name('view');
            Route::post('extend', 'UserLicenseController@extend');
            Route::post('upgrade', 'UserLicenseController@upgrade');
        });
    });
});

Route::get('{any}', function () {
    throw new NotFoundHttpException();
})->where('any', '.*');
